\documentclass[a4paper, notitlepage]{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\begin{document}

\title{MSc thesis: project planning\\version 1.0} 
\author{Zmicier Zaleznicenka, MSc CS student, \#4134575}
\date{\today}
\maketitle

\setcounter{secnumdepth}{0}

\section{Synopsis}

This document contains initial planning for the Master's thesis project "Detecting performance degradations of large software projects". The scope of this project is research of the methods for automating the analysis of performance regression tests. Nowadays, performance engineering involves a lot of manual processing of testing results which is a long and elaborative process. Existing manual performance engineering techniques do not allow performance analysts to conduct and analyse all the tests they want due to a large number of performance parameters to be considered during the analysis and time pressure, usual for quality assessment tasks \cite{Ngu}\cite{Yil}. The discussed project is meant to be completed at TomTom International BV and includes development of a web system maintaining a performance repository for the company's applications and allowing to mine it to reveal correlations between performance parameters over time. Such a system will help the software developers to automatically detect performance regressions between the different releases of their software. Studying automatic detection of performance degradations in the evolving software is important for software development industry, because advances in this field would help to improve control over the evolving software and its performance. Such improvements would allow to decrease time and money spent on performance regression testing and on software development processes at whole. The project aims at improving performance engineering processes in distributed systems accessible from the Web and consisting of several interdependent components. 

A preliminary literature study was conducted by the author prior to starting the project. During this study, there were found two methods for automating performance regressions detection that show promising results and are described in scientific literature. These methods are mining performance testing repositories with associative rules \cite{Foo} and employing control charts technique \cite{Ngu}. A number of other approaches to this problem, that were described by the author in his survey, have severe drawbacks and seem less applicable to the project scope. It was decided to work on improving and extending the performance testing repositories mining with associative rules method. This method was initially applied in this context by Foo in \cite{Foo}. This project aims to continue the research started in \cite{Foo}.

It was decided to work on further improving association rules mining technique as it is a rather simple and flexible method. It demonstrates good results being able to detect both obvious performance degradations that are artificially introduced to the software and minor performance regressions that can be missed by performance analysts during manual analysis of performance testing results.

Apart from suggesting the analysis methods for detecting performance regressions, this approach assumes building a performance regression repository to store the results of the passed performance tests. However, the authors of \cite{Foo} did not provide a working implementation of such a repository. Thus, one of the tasks to be completed within the scope of the project is building an open-source implementation of a performance testing repository supporting several different formats of performance testing files. The repository should have an interface allowing to import raw test results, convert them to unified format and provide methods for using these results for the analysis. Having such a repository will allow the performance analysts to keep track of the changes in the software performance over time and automatically analyse new performance tests based on the previous runs.

\section{Research questions}

The following research questions will be addressed while working on the project.

\begin{itemize}
\item \emph{How to identify performance degradations and their causes in the large environments? How effective is it to use performance testing repositories mining for this task?}
	\begin{itemize}
	\item \emph{How to store the results of performance tests from various sources for the further analysis?}
	\item \emph{How to detect performance degradations in a performance test run having the results of the previous runs in a performance testing repository? Is association rules mining a suitable technique for this task?}
	\item \emph{What useful information can be extracted from a performance testing repository and what data mining techniques can be used for its retrieval and analysis?}
	\end{itemize}
\item \emph{How to compare the changes in software performance between two runs in the different environments or after changing hardware or software setup?}
\item \emph{How to detect software performance degradations in one environment having performance metrics from another environment? Are ensemble-learning algorithms suitable for this purpose?}
\item \emph{How to represent the results of automatic performance testing analysis to provide the analysts with the detected performance degradations and their justification?}
\end{itemize}

\section{Contribution}

The main contributions of the project after its completion should be the following.

\begin{itemize}
\item A publicly available open-source system intended for performance regression testing of web systems based on mining performance regression repositories.
\item Analysis of different machine-learning algorithms by their applicability to finding correlations in performance metrics.
\item Extension of Foo's work (\cite{Foo}) on distributed systems.
\end{itemize}
   
\section{Planning and staging}

The project will last from March, 15 till November, 15, 2013. The project will include the following stages:

\begin{enumerate}
\item Acquaintance with the software written at the department, existing tests and deployment procedures. Understanding performance engineering processes in a company. Time frame: 2 weeks. Deliverable: a document, describing the system and testing processes (to be used further in the Master's thesis document).
\item Development of a general framework for maintaining the performance testing repository. It should contain a database with performance tests and an interface to it allowing to import raw data and export testing results. Time frame: 3 weeks. Deliverable: source code with implementation of a performance testing repository.
\item Development of association rules analysis module. This module should process collected performance testing data, generate associative rules and find correlations in testing data. Time frame: 4 weeks. Deliverable: source code with implementation of analysis module.
\item Installation and performance testing of an open-source web framework (i.e. Dell DVD Store) to prove validity of a built system. Time frame: 2 weeks. Deliverable: a document describing performance testing of such a framework using the built tool.
\item Adaptation of the existing performance test suite and creation of additional tests to assess performance of company's applications. Time frame: 2 weeks. Deliverable: a set of performance tests.
\item Performance analysis of the company's applications using a built set of tests and a tool. Consultancies with the performance analysts and tuning of the system based on their feedback. Time frame: 4 weeks. Deliverable: a performance report covering the company's infrastructure.
\item Implementation of ensemble-learning approach \cite{Foo} to support performance testing at heterogeneous environments and its testing. Time-frame: 4 weeks. Deliverable: an ensemble-testing module. 
\item Fine-tuning the built system using various discretisation strategies and associative rules generation algorithms. Time frame: 4 weeks. Deliverable: a document discussing differences between the applied algorithms for the accuracy of performance testing.
\item Writing Master thesis report. Time frame: 5 weeks. Deliverable: Master's thesis.
\end{enumerate}

\section{Risk analysis}

The following events may have impacts on the planning.

\begin{itemize}
\item One of the important tasks for the project is building a performance repository to gather performance metrics in unified format from various sources. As many different performance testing tools are used in the company and some of them are proprietary, it may take longer than expected to implement algorithms processing the outputs of these tools. Instead of using the results of these tools in some situations it might be needed to re-create testing processes using different software with flexible exporting policies. This may lead to bigger time investments in steps 2 and 5.

\item Building an analysis module and its further fine-tuning using various algorithms might take more time than expected because there are many algorithms available that can be applied to the field and finding their proper combination may take a while.

\item Testing and analysing stage may take a lot of time due to the high load of the company test servers and complicated deployment procedures. However, if the research and implementation stages are done in time, there should be enough time left for testing. 
\end{itemize}

\begin{thebibliography}{9}
\bibitem{Foo} King Chun Foo. Automated discovery of performance regressions in enterprise applications. Master’s thesis, Queen’s University, Kingston, ON, Canada, January 2011.
\bibitem{Ngu} Thanh H. D. Nguyen, Bram Adams, Zhen Ming Jiang, Ahmed E. Hassan, Mohamed N. Nasser, and Parminder Flora. Automated detection of performance regressions using statistical process control techniques. In \emph{Proc. ICPE’12}, pages 299–310, New York, NY, USA, 2012. ACM.
\bibitem{Yil} Cemal Yilmaz, Arvind S. Krishna, Atif M. Memon, Adam A. Porter, Douglas C. Schmidt, Aniruddha S. Gokhale and Balachandran Natarajan. Main effects screening: A distributed continuous quality assurance process for monitoring performance degradation in evolving software systems. In \emph{Proc. 27th Int’l Conf. on Software Engineering (ICSE’05)}, pages 293–302, St. Louis, MO, USA, 2005.
\end{thebibliography}

\end{document}