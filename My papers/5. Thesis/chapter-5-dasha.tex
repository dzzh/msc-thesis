\chapter{\label{cha:dasha}Dasha: a performance testing mining algorithm}

In this chapter a new data mining algorithm called Dasha is presented. This algorithm compares the patterns existing in transaction databases with association rule mining techniques. In Dasha, the datasets are initially compared using frequent itemsets that are derived from them. Only after the itemsets are compared and their difference is obtained, this difference is used to construct association rules for the further analysis. This two-level comparison approach based on traversal of lexicographic tree structures allows to compare large performance testing datasets in reasonable time and is suitable for usage in industrial setting\footnote{The implementation of the Dasha algorithm as a web application including the performance repository is available at \emph{https://bitbucket.org/dzzh/mining}. A stand-alone algorithm's implementation is available at \emph{https://bitbucket.org/dzzh/python-dasha}.}.

\section{\label{sec:dasha_overview}Dasha overview}

Taking into account considerations listed in the previous chapter, the Dasha algorithm was designed with the emphasis on performance. As it was pointed out earlier, the number of association rules extracted from the performance databases can be extremely large. Most of these rules are not interesting and are contained in longer rules. Their evaluation leads to significant time consumption even for rather small databases. To mitigate this problem, data analysis in Dasha is initially performed not on the association rules, but on the frequent itemsets. 

In the proposed algorithm a tree structure to store the information about the frequent itemsets and their supports is used. This tree serves as a caching layer for itemsets-related information and implements a data structure initially described by Rymon as a \emph{set-enumera\-tion tree} in \cite{Rymon}. Prior to our study, set-enumeration trees have already been used for association rule mining by a number of researchers (see e.g. \cite{Agarwal,Bayardo,Burdick}). However, in the preceding studies such trees were mainly used to store information about the transactions contained in the source datasets for further generation of frequent itemsets. Agarwal et al. in \cite{Agarwal} use set-enumeration trees to store itemset-related data, but their implementation significantly differs from their approach presented in this paper. A contribution of our approach is an idea to construct a set-enumeration tree from the already retrieved frequent itemsets and use the resulting data structure for quick traversal of the itemsets, retrieval of their supports and fast comparison of two sets of frequent itemsets. 

Later in this paper the set-enumeration tree structure used for storing frequent itemsets is referred to as \emph{the support tree}, because it stores supports for the combinations of items existing in the datasets under study.

The Dasha algorithm to detect performance degradations consists of the following steps.

\begin{enumerate}
\item Build the support trees for baseline and target datasets. 
\item Compare the support trees using level-wise top-down approach.
\item Derive the association rules from the different frequent itemsets.
\item Analyze the association rules and make conclusions.
\end{enumerate}

In the following sections these steps are described in more details and the properties of the resulting algorithm are explored.

\section{\label{sec:dasha_sample-datasets}Sample datasets}

To illustrate the flow of the Dasha algorithm and the data structures used in it, throughout this chapter the algorithm's execution is demonstrated on a simple example. In this section the source data used for it is described.

Assume a performance analyst wants to find difference in performance between two test runs. The first run is performed on a stable version of the software and has satisfactory performance. It is considered to be a baseline. The second run is performed on a new version of the software and its performance has to be evaluated. Both test runs consist of six observations, during each observation four performance counters are tracked. The data is classified and stored in a performance repository. Thus, each counter value is associated with a category, either \emph{Low} (1), \emph{Medium} (2) or \emph{High} (3)\footnote{See Section \ref{ss:repo_classification} for more details about the classification algorithms.}. The counter values are hashed to improve the algorithm's performance\footnote{See Section \ref{ss:arm_hashing} for more details about the hashing function.}. As a result, each counter observation in the datasets has value between 11 and 43, where tens mean the ids of the performance counters, ones mean categories. E.g. 12 means \emph{Medium} value for counter \emph{c1}, 43 means \emph{High} value for counter \emph{c4}. The performance datasets contents after these optimizations are presented in Table \ref{table:dasha_sample-datasets}. 

\begin{table}[ht]
  \centering
  \subbottom[Baseline]{
    \begin{tabular}{ | c | c | c | c | c | }
      \hline
      \textbf{TID} & \textbf{c1} & \textbf{c2} & \textbf{c3} & \textbf{c4} \\
      \hline
      1 & 12 & 22 & 32 & 42 \\ \hline
      2 & 12 & 22 & 32 & 42 \\ \hline
      3 & 12 & 23 & 32 & 43 \\ \hline
      4 & 12 & 22 & 32 & 42 \\ \hline
      5 & 12 & 22 & 32 & 42 \\ \hline
      6 & 12 & 23 & 32 & 43 \\ 
      \hline
    \end{tabular}
  }
  \quad
  \subbottom[Target]{
    \begin{tabular}{ | c | c | c | c | c | }
      \hline
      \textbf{TID} & \textbf{c1} & \textbf{c2} & \textbf{c3} & \textbf{c4} \\
      \hline
      1 & 12 & 22 & 32 & 42 \\ \hline
      2 & 12 & 23 & 32 & 43 \\ \hline
      4 & 12 & 22 & 32 & 42 \\ \hline
      3 & 12 & 23 & 32 & 43 \\ \hline
      5 & 12 & 23 & 32 & 41 \\ \hline
      6 & 12 & 22 & 32 & 42 \\ 
      \hline
    \end{tabular}
  }
  \caption{Sample datasets.}
  \label{table:dasha_sample-datasets}
\end{table}

As this example is trivial, the conclusions about differences in data can be made manually. First of all, it is obvious that counters \emph{c1} and \emph{c3} are stable in both datasets. In every observation they have \emph{Medium} value that is encoded as 12 and 32 respectively. Counter \emph{c2} has \emph{High} value in two observations in the baseline dataset and in three observations in the target dataset. It has \emph{Medium} value in four observations in the baseline dataset and in three observations in the target dataset. Counter \emph{c4} behaves similarly to \emph{c2} in the baseline but has different behavior in the target dataset. It has \emph{Low} value in one observation, \emph{Medium} value in three observations and \emph{High} value in two observations. Depending on the algorithm's settings (i.e. \emph{minsup} and \emph{mindiff}) these differences can either be reported as violations or not. 

\section{\label{sec:dasha_support-tree-gen}Support tree structure and generation}
Before discussing the algorithm's implementation, the support tree that is an important data structure used in Dasha has to be described.

This tree stores the information about the frequent itemsets existing in the studied database and their supports. It is assumed that the items within a frequent itemset do not repeat and there is total ordering $<_O$ of the items in the database. If item $i$ occurs before item $j$ in the ordering, this is denoted by $i<_Oj$. 

The support tree closely resembles the FP-tree used for frequent itemsets generation in the FP-growth algorithm described in \cite{Han00}\footnote{A short introduction into the FP-growth algorithm and the FP-tree data structure is provided in section \ref{ss:arm_fpgrowth}.}, but differs from it in the following aspects.

\begin{itemize}
\item The FP-tree is used to organize a list of transactions. The support tree is used to organize a list of frequent itemsets.
\item The support tree does not have a concept of an item-header table.
\end{itemize}

In other aspects, the support tree resembles the FP-tree and can be built following the procedure similar to building the FP-tree. Total ordering in the support tree is also implemented similarly to the ordering used in the FP-tree. Namely, the items in an itemset to be added to the support tree are first sorted by their frequency in decreasing order, then naturally in increasing order. Later in this document this ordering is referred to as \emph{FP-ordering}.

More formally, the support tree can be defined as follows\footnote{Tree definition and algorithm's description are based on the FP-tree description as presented in \cite{Han00}.}.

\begin{samepage}
\textbf{Definition 1 (Support tree).} A \emph{support tree} is a data structure defined below.

\begin{enumerate}
\item Support tree is a tree structure. It consists of a root node containing \emph{null} as an item and a set of item-prefix subtrees as the children of the root.
\item Each node in the item-prefix subtree contains an underlying item, support value, link to a node parent and a set of links to node children.
\item Support value of a node is used to store the support of an itemset that appears in the respective dataset and includes the node and all its parents up to the root.
\item For two nodes $i$ and $j$ in the support tree it holds that if $i.parent == j$, then $j.item <_O i.item$ and $i.support \le j.support$.
\end{enumerate}
\end{samepage}

Based on this definition, a support tree can be constructed using the following algorithm.
\newline\newline
\emph{Input:} a list of itemsets.\newline
\emph{Output:} an instance of a support tree.\newline

Initialize a support tree with a root $R$ associated with \emph{null} item. For each frequent itemset in the input list do the following.
\begin{enumerate}
\item Sort the items appearing in the frequent itemset in the FP-order using $<_O$ relation. Let the list of items in a current itemset be $[p|P]$, where $p$ is the first element and $P$ is the remaining list. 
\item Add the items into the tree using $R.add([p|P])$ procedure that works as follows. If $R$ has a child $n$ such that $n.item == p.item$, remember this node; otherwise create a new node $n$ and link its parent to $R$. Then, if $P$ is non-empty, call $n.add(P)$; otherwise, set $n.support$ to itemset support.
\end{enumerate} 

\begin{center}
\line(1,0){250}
\end{center}

During the tree construction process, the itemset supports are assigned to the last nodes of the respecting paths in the tree. If the source list includes all the frequent itemsets that exist in the database, the resulting support tree will not have nodes with no support associated with them, except for the root. 

\section{Dasha implementation}

In this section the steps used in the Dasha algorithm are discussed in more details.

\subsection{Support trees generation}

\emph{Input:} transaction database, minsup, mindiff.\newline
\emph{Output:} support tree.

\subsubsection{\label{sss:dasha_fi_gen}Frequent itemsets generation and support trees construction}

The first step of the Dasha algorithm to compare the patterns in transaction databases is to construct the support trees for both baseline and target datasets.

As the support tree construction is based on frequent itemsets processing, they have to be generated from the database in advance. To complete this task, any existing frequent itemsets mining algorithm can be used. In this study it was decided to use an implementation of the FP-growth algorithm \cite{Han00}. This algorithm was chosen because of the structure of performance testing datasets that was described in the previous chapter. This structure imposes the existence of large frequent itemsets in the database and the FP-growth should be more efficient for generating them than any of Apriori-based algorithms. Also, the FP-trees that are generated by the FP-growth algorithm are used at the later steps in Dasha for calculating itemset supports.

After a list of frequent itemsets with their supports is retrieved from the database, it is used to build a support tree as described in section \ref{sec:dasha_support-tree-gen}.

\subsubsection{\label{sss:dasha_minsup_decrease}Decreasing minimum support value for the target tree}

Before generating a support tree for the target dataset, it is needed to decrease its minimum support to include in the resulting support tree the itemsets that have a little lower support than the ones in a baseline dataset. This change is important for a later step when the support trees are compared. If not decreasing target's minimum support, there will be situations when an itemset $A$ has support slightly higher than \emph{minsup} in the baseline dataset and slightly lower than \emph{minsup} in the target dataset. In this case this itemset should appear in the baseline support tree but it will be missing from the target support tree and during the comparison this will be reported as a false positive violation. However, if the target dataset would contain the itemsets with support lower than \emph{minsup}, itemset $A$ will be presented in it and the comparison of these itemsets will be performed based on the difference between their support values, that can be considered insignificant. In this situation the false violation will not be reported.

It is important to properly select the decreased value for target's minimum support. If this value is chosen to be too close to the \emph{minsup} threshold specified by the user, it will not solve the problem. If target's \emph{minsup} is chosen to be too low, it will lead to the drastic drop of the algorithm's performance, as when \emph{minsup} decreases, the number of frequent itemsets increases and it takes longer to generate them. The following formula was used in this study to calculate \emph{minsup} value for the target dataset.

\begin{center}
$minsup_{target}=minsup*(1-mindiff)$
\end{center}

Here, \emph{minsup} is minimum support threshold measured in the number of transactions and \emph{mindiff} is minimum difference threshold measured from 0 to 1. These values are defined by user. The meaning of \emph{mindiff} is explained in section \ref{ss:dasha_support-comparison}. If the number of transactions in baseline and target datasets differ, the formula has to be adjusted to reflect this difference.

\subsubsection{Implementation}

The pseudocode implementation of the support trees generation algorithm is presented in Listing \ref{lst:build-trees}.

\lstinputlisting[language=Python,caption=Support trees construction., label={lst:build-trees}]{code/dasha-build-trees.py}

\subsubsection{Example}

The resulting support trees generated for the sample datasets with the Dasha algorithm are presented at Figure \ref{fig:dasha_support-trees}. A set of paths in each support tree equals to the set of frequent itemsets for each of the datasets. The trees were generated with $minsup=2$ and $mindiff=10\%$. Each node in the resulting tree contains a value from the dataset and its support in parenthesis. The support is calculated for the itemset containing the node itself and all its parents up to the root.

In this example the target support is not adjusted as both \emph{minsup} and \emph{mindiff} values are rather small. With such settings decreasing target support is not helpful.

From the support trees it can easily be seen that the itemsets $\{12,22\}$, $\{12,23\}$, $\{42\}$, $\{43\}$ and some others have different supports and thus represent potential violations. Whether these differences will be reported as violations or not depends on the algorithm's settings (\emph{minsup}, \emph{mindiff}). 

Shaded nodes in the baseline support tree illustrate the roots of the subtrees that differ from target. They are explained in the following section. 

\begin{figure}
\centering
\subbottom[Baseline]{  
\tikzstyle{every node}=[anchor=west]
\tikzstyle{border}=[fill=black!20]
\begin{tikzpicture}[%
  grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
  edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)},
  scale=0.95]
  \node {root}
    child { node {12(6)}
      child { node [border] {22(4)}
        child { node {42(4)}}
      }
      child [missing] {}
      child { node [border] {23(2)}
        child { node {43(2)}}
      }
      child [missing] {}      
      child { node {32(6)}
        child { node [border] {22(4)}
          child { node {42(4)}}
        }
        child [missing] {}    
        child { node [border] {23(2)}
          child {node {43(2)}}
        }
        child [missing] {}
        child { node [border] {42(4)}}
        child { node {43(2)}}
      }
      child [missing] {}
      child [missing] {}
      child [missing] {}      
      child [missing] {}
      child [missing] {}            
      child [missing] {}      
      child { node [border] {42(4)}}
      child { node {43(2)}}
    }
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}      
    child { node [border] {22(4)}
      child { node {42(4)}}
    }
    child [missing] {}    
    child { node [border] {23(2)}
      child { node {43(2)}}
    }  
    child [missing] {}      
    child { node {32(6)}
      child { node [border] {22(4)}
        child { node {42(4)}}
      }
      child [missing] {}    
      child { node [border] {23(2)}
        child { node {43(2)}}
      }
      child [missing] {}    
      child { node [border] {42(4)}}
      child { node {43(2)}}
    }   
    child [missing] {}    
    child [missing] {}        
    child [missing] {}    
    child [missing] {}        
    child [missing] {}
    child [missing] {}    
    child { node [border] {42(4)}}
    child { node {43(2)}};
\end{tikzpicture}}
\quad
\subbottom[Target]{
\tikzstyle{every node}=[anchor=west]
\begin{tikzpicture}[%
  grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
  edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)},
  scale=0.95]
  \node {root}
    child { node {12(6)}
      child { node {22(3)}
        child { node {42(3)}}
      }
      child [missing] {}
      child { node {23(3)}
        child { node {43(2)}}
      }
      child [missing] {}      
      child { node {32(6)}
        child { node {22(3)}
          child { node {42(3)}}
        }
        child [missing] {}    
        child { node {23(3)}
          child {node {43(2)}}
        }
        child [missing] {}
        child { node {42(3)}}
        child { node {43(2)}}
      }
      child [missing] {}
      child [missing] {}
      child [missing] {}      
      child [missing] {}
      child [missing] {}            
      child [missing] {}      
      child { node {42(3)}}
      child { node {43(2)}}
    }
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}      
    child { node {22(3)}
      child { node {42(3)}}
    }
    child [missing] {}    
    child { node {23(3)}
      child { node {43(2)}}
    }  
    child [missing] {}      
    child { node {32(6)}
      child { node {22(3)}
        child { node {42(3)}}
      }
      child [missing] {}    
      child { node {23(3)}
        child { node {43(2)}}
      }
      child [missing] {}    
      child { node {42(3)}}
      child { node {43(2)}}
    }   
    child [missing] {}    
    child [missing] {}        
    child [missing] {}    
    child [missing] {}        
    child [missing] {}
    child [missing] {}    
    child { node {42(3)}}
    child { node {43(2)}};
\end{tikzpicture}}
\caption{Support trees for the sample datasets.}
\label{fig:dasha_support-trees}
\end{figure}

\subsection{\label{ss:dasha_support-comparison}Support trees comparison}

\emph{Input:} Support trees and FP-trees for baseline and target, mindiff.\newline
\emph{Output:} a list of violating itemsets.

\subsubsection{\label{sss:dasha_comp-alg-desc}The comparison algorithm's description}

After the baseline and target support trees are constructed, they have to be compared to find the difference in patterns appearing in the respective datasets.

To find the difference between the support trees, a top-down breadth-first approach is used. The comparison starts with the children of the root node in the baseline support tree. These nodes are added to the comparison queue and until the queue is empty the nodes are retrieved from it one by one and processed.

Once a baseline tree node is retrieved from a comparison queue, the algorithm looks for the corresponding node in the target tree. The support tree is constructed in such a way that every node in it represents an itemset including the node itself and all its parents up to the root. The support value of a node equals to the support of this itemset. Hence, to find a node $A_T$ in a target tree that corresponds to a node $A_B$ in a baseline tree, it is needed to move up the baseline tree to reconstruct an ordered itemset $A$ and find this itemset in the target tree by moving down from the root.

If a lookup procedure is not able to find $A_T$ node, this is considered a violation. In this case, even if $A_B$ node has any children, the lookup process for the subtree having $A_B$ as a root terminates, because all the itemsets identified with $A_B$'s children contain $A_B$'s item, and if a target tree does not contain $A_T$, it neither contains any itemsets including its item, which is the same as $A_B$'s item. 

If $A_T$ is found, the algorithm compares support values of $A_B$ and $A_T$. Before the comparison, if the number of transactions in the baseline and target datasets differs, the support value in the target tree has to be properly scaled to reflect this difference.

To compare the support values for the support tree nodes, minimum difference value is used as a threshold. If $\frac{|support_{A_B}-support_{A_T}|}{support_{A_B}}$ exceeds \emph{mindiff}, it is assumed that the values are significantly different and this is a violation. In this case, the algorithm reacts the same way as if $A_T$ is missing. Any $A_B$'s children are not processed further.

If the difference between $A_B$ and $A_T$ is less that \emph{mindiff}, it is assumed that pattern $A$ matches in the support trees. In this case, all $A_B$'s children are added to the queue.

After the queue is empty, the algorithm reports a list of violating itemsets detected when processing the items in the queue. These itemsets are grouped by their parent non-violating itemsets (itemset $\{a,b\}$ is called a parent of FP-ordered itemset $\{a,b,c\}$ here and later in this study).

\subsubsection{Implementation}

Pseudocode of the support trees comparison algorithm can be found in Listing \ref{lst:compare-trees}.

\lstinputlisting[language=Python,caption=Support trees comparison., label={lst:compare-trees}]{code/dasha-compare-trees.py}

\subsubsection{\label{sss:dasha_support-comparison-example}Example}

At Figure \ref{fig:dasha_support-trees} there are the support trees that are built from the sample baseline and target datasets presented in section \ref{sec:dasha_sample-datasets}. The shaded nodes in the baseline trees illustrate the difference between the trees that is found if the algorithm is run with $minsup=2$ and $mindiff=10\%$. If the difference is found for a node, the algorithm does not go further to analyze its children. 

The results of comparing the support trees for the sample datasets are presented in Listing \ref{lst:support-trees}.

\begin{lstlisting}[caption=Comparison of the support trees for sample datasets.,label={lst:support-trees}]
Shared path: [] (supports 6, 6)
  node 22 (supports 4, 3)
  node 23 (supports 2, 3)
  node 42 (supports 4, 3)

Shared path: [12L] (supports 6, 6)
  node 22 (supports 4, 3)
  node 23 (supports 2, 3)
  node 42 (supports 4, 3)

Shared path: [32L] (supports 6, 6)
  node 22 (supports 4, 3)
  node 23 (supports 2, 3)
  node 42 (supports 4, 3)

Shared path: [12L, 32L] (supports 6, 6)
  node 22 (supports 4, 3)
  node 23 (supports 2, 3)
  node 42 (supports 4, 3)
\end{lstlisting}

\subsection{Generation of association rules and their analysis}

\emph{Input:} a list of violating itemsets, FP-trees, mindiff.\newline
\emph{Output:} a list of association rules grouped by antecedent.

\subsubsection{\label{sss:dasha_assoc-rules}Association rules generation}

After a list of violating itemsets is retrieved, it is used for association rules generation. The itemsets presented in the input list significantly differ from their parents and this allows to conclude that the association rules generated out of the violating itemsets should reflect this difference. For each violating itemset presented in the input list and containing $n$ items, the Dasha algorithm generates $n$ association rules having $n-1$ items in the antecedent and one distinct item in the consequent. For each of the generated rules, the algorithm calculates its support and confidence. For rule $[a,b] \rightarrow c$, support is calculated as $support_{[a,b,c]}$, confidence is calculated as $\frac{support_[a,b]}{support_{[a,b,c]}}$.

An important challenge to be addressed by the algorithm is fast calculation of rule confidences. When the support of a rule can be found in the respective support tree, the support for the rule antecedent is not necessarily presented there. For example, if the algorithm finds a violating itemset $\{a,b,c\}$, it should generate three association rules out of it, namely

\begin{center}
$[a,b] \rightarrow c$,

$[a,c] \rightarrow b$,

$[b,c] \rightarrow a$.
\end{center}

Obviously, depending on the source data the itemsets $\{a,c\}$ and $\{b,c\}$ may be infrequent and thus missing from the respective support tree. A naïve approach to calculate their supports would be to count their occurrences in the source datasets. However, this would drastically decrease the algorithm's performance. Instead, the Dasha algorithm retrieves these data from the corresponding FP-trees. To do it, the algorithm takes the least frequent item from the itemset, finds all its appearances in the FP-tree using its node-links and moves up to the root starting from these nodes. If the algorithm passes all the itemset nodes on its way to the root, it concludes that the respective path is valid. Then it summarizes the count values for the least frequent nodes in the valid paths. By the FP-tree design, the resulting value equals to the itemset support in the respective dataset. 

It is needed to mention that if the itemset is presented in the respective support tree, its support should be retrieved from there, not from the FP-tree, as the FP-tree lookups described in the previous paragraph are much more computationally intensive than search in a support tree. The FP-tree lookups should only be used if an itemset is not found in the support tree.

A list of association rules generated by the Dasha algorithm for the sample datasets is presented in Listing \ref{lst:association-rules}. The association rules generated at this step are grouped by their antecedent and are subject to additional optimizations that are discussed in the following section. 

\subsubsection{\label{sss:dasha_assoc-rules-analysis}Association rules analysis and selection}

The resulting list of association rules reflects the most important differences between the baseline and target datasets. Level-wise top-down comparison of frequent itemsets implemented with set-enumeration trees has allowed to limit the number of the association rules that are returned. However, the number of these rules can be decreased even more. The algorithm generates many rules with the same consequents and different antecedents. To detect a violation for the node represented by the association rule consequent, the amount of the returned data may be excessive. To address this issue, the algorithm chooses the most representative rule per different consequent instead of returning all the association rules generated at the previous step. The selection criteria is average difference between baseline and target supports for the categories of a given counter in a rule (called \emph{rule severity}). More formally, it is measured as \(\frac{\sum_{i=1}^{c}|c_i.bsl\_sup-c_i.tgt\_sup|}{r.bsl\_sup+r.tgt\_sup}\), where $r$ is an association rule, $c$ \nolinebreak-- rule consequents for the violating performance counter. If severity metric is almost the same for the two rules, the rule with larger baseline support is selected for further reporting. For example, if the algorithm generates three association rules for consequent $d$, namely 
\begin{center}
$[a] \rightarrow d$ (severity 0.15, baseline support 0.4), 

$[a,b] \rightarrow d$ (severity 0.2, baseline support 0.3), 

$[a,b,c] \rightarrow d$ (severity 0.2, baseline support 0.2), 
\end{center}
the second rule will be chosen for reporting as it has higher severity than the first rule and higher baseline support than the third rule. In section \ref{sss:dasha_assoc-rules-example} the result of applying the same heuristic to the association rules generated from the sample datasets is shown.

After the most descriptive association rule is chosen for each of the violating performance counters, it makes sense to also present the distribution of categories for this counter. This can be helpful in making conclusions about the violation -- whether the counter values in the target dataset increased or decreased in comparison with the baseline. This can be done using the FP-tree traversals similarly to calculating itemset supports as described in section \ref{sss:dasha_assoc-rules}. The traversal will return all the categories of a given rule consequent and their supports. When returning these data, some categories with low supports in baseline and target can be dropped to emphasize the most important categories. 

Listing \ref{lst:final-output} shows the output of the Dasha algorithm for the sample datasets after the performed optimizations. This is the final output of the algorithm that is used by the report generation engine described in the following chapter.

\subsubsection{Implementation}

The pseudocode implementation of the association rules generation algorithm is presented in Listing \ref{lst:build-rules}. The implementation of the rules selection algorithm is presented in Listing \ref{lst:analyze-rules}.

\lstinputlisting[language=Python,caption=Association rules generation., label={lst:build-rules}]{code/dasha-build-rules.py}

\lstinputlisting[language=Python,caption=Association rules analysis., label={lst:analyze-rules}]{code/dasha-analyze-rules.py}

\subsubsection{\label{sss:dasha_assoc-rules-example}Example}

Association rules generated for the sample datasets are presented in Listing \ref{lst:association-rules}.

\begin{lstlisting}[caption=Association rules for sample datasets., label={lst:association-rules}]
Antecedent: [] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)

Antecedent: [32L] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)

Antecedent: [12L] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)

Antecedent: [12L, 32L] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)
\end{lstlisting}

After performing the optimizations described in section \ref{sss:dasha_assoc-rules-analysis}, the output of the Dasha algorithm looks as presented in Listing \ref{lst:final-output}. The optimizations performed to the association rules as they are shown in Listing \ref{lst:association-rules} include the following.

\begin{itemize}
\item Rules with duplicating consequents are removed. The resulting output contains only the rules with different consequents. 
\item For each violating counter the resulting output contains all possible categories that exist in the source datasets (see rule $[12,32]\rightarrow41$, which is missing in Listing \ref{lst:association-rules} due to its low support and rule $[12,32]\rightarrow43$ which is missing from Listing \ref{lst:association-rules} as it is considered matching in the datasets).
\end{itemize}

\begin{lstlisting}[caption=Final output of the Dasha algorithm for sample datasets.,label={lst:final-output}]
Antecedent: [12L, 32L] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)

Antecedent: [12L, 32L] (supports 6, 6)
 consequent 41: supports (0, 1), confidences (0.00, 0.17)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)
 consequent 43: supports (2, 2), confidences (0.33, 0.33)
\end{lstlisting}

\subsection{Putting it all together}

Source code of the Dasha algorithm can be found in Listing \ref{lst:dasha-algorithm}. It contains the functions described in the previous listings earlier in this chapter.

\lstinputlisting[language=Python,caption=Dasha algorithm to compare patterns in transaction databases., label={lst:dasha-algorithm}]{code/dasha.py}

\section{Summary}

Dasha is a new data mining algorithm for fast detection of differences in transaction databases using association rule mining techniques. This algorithm was designed to compare the results of performance test runs, but can also be applied in other domains. For example, it is expected that the presented algorithm will be effective in comparative market basket analysis for detecting changes in customer behavior between different locations or over time. However, this assumption has to be justified with a case study that was not performed in the scope of this project.


