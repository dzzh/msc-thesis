\chapter{\label{cha:case_study_dell}Case study: Dell DVD Store}

Dell DVD Store\footnote{\emph{http://linux.dell.com/dvdstore/}} (DS2) is an open-source web benchmark that is often used by academic researchers and software developers in industry to performance test web software and server hardware. This is a rather simple web application that is intended to simulate a web store allowing its customers to browse and purchase DVDs. DS2 consists of a backend database layer, a web application layer and driver programs. The database layer contains information about the registered users, available DVDs and fulfilled orders. During the setup process, it can be populated with automatically generated data of arbitrary size. Web application layer allows to browse the store and make purchases using a web browser. Driver program simulates the load to an application following the patterns common for web store customers.

\section{Testing setup}

\subsection{Case study goals and hypotheses}

\paragraph{Goal 1} To apply the automatic performance detection approach presented in Chapter \ref{cha:dasha} to a relatively simple example.

\paragraph{Goal 2} To study the effects of changing the algorithm's settings on the returned results.

\paragraph{Goal 3} To illustrate the workflow of a performance engineer during the analysis of the performance tests using the automatic performance regressions detection tool.

\paragraph{Hypothesis 1} No violations should be detected between two performance test runs that are executed against the same version of the application under study in same conditions.

\paragraph{Hypothesis 2} If a new version of the software under study contains a performance degradation, violations of the tracked performance counters will be raised after comparing the performance tests results with those captured while testing a baseline version of the software.

\subsection{Hardware and software}

The hardware setup for this case study consisted of three computers. Dell DS2 was installed on a dedicated Linux server with an automatically generated database of 20 GB size. The database size was chosen to be several times larger than the amount of available RAM. This led to quite large read and write times both at database and disk system levels and resulted in clear performance testing results. A load generator was run from a laptop located in the same network as the target machine. The communication between two machines was set up using Wi-Fi connection. The values of the performance counters gathered at the target machine were stored in OpenTSDB time-series database\footnote{\emph{http://opentsdb.net}} instance that was deployed in a virtual machine in the cloud.

Dell DVD Store includes several different database backends and web layer implementations. For this case study, the application was configured to work with MySQL database and PHP web layer deployed at Apache web server. 

To have more control over the load generator and access the latencies of the issued requests, instead of using a default load driver supplied with DS2 benchmark, a performance test plan was created with Apache JMeter\footnote{\emph{http://jmeter.apache.org}} tool. The flow of this test plan resembled one of the default load driver and consisted of various user activities including login, registration, browsing and purchasing. All the performance test runs discussed in the chapter were run with 10 threads. Half of the users issuing requests to DS2 were already registered and had to log in, the other half had to register first. Each user performed five different searches on average and each second user issued a request to buy some DVDs in the store after the searches.

\subsection{Tracked performance counters}

To analyze performance of a target application, 17 performance counters were tracked and analyzed in the research prototype consisting of a performance testing repository\footnote{See Chapter \ref{cha:perfrepo} for performance testing repository description.} and an analysis module based on the Dasha algorithm\footnote{See Chapter \ref{cha:dasha} for the Dasha algorithm's description.}. These counters are overviewed in Table \ref{table:dell_counters}. Request latencies were gathered at a client machine with a load generator. Contrary to the values of the other counters, these values were gathered upon receiving a specific request. The other counters were gathered at a target machine with sampling periods of 15 or 60 seconds.

\begin{table}
  \begin{center}
    \begin{tabular}{ | c | c | c | }
  	  \hline
  	  \textbf{Category} & \textbf{Counter} & \textbf{Sampling period, sec} \\
      \hline
      \multirow{5}{*}{CPU} & User load & 15 \\ \cline{2-3}
      & System load & 15 \\ \cline{2-3}
      & I/O wait & 15 \\ \cline{2-3}
      & Num interrupts & 15 \\ \cline{2-3}      
      & Num context switches & 15 \\ \hline
      \multirow{2}{*}{I/O system} & Read requests & 60 \\ \cline{2-3}
      & Write requests & 60 \\ \hline
      \multirow{2}{*}{MySQL} & Bytes received & 15 \\ \cline{2-3}
      & Bytes sent & 15 \\ \hline
      \multirow{7}{*}{Request latencies} & Browse actor & - \\ \cline{2-3}
      & Browse category & - \\ \cline{2-3}
      & Browse title & - \\ \cline{2-3}      
      & Login & - \\ \cline{2-3}
      & Register & - \\ \cline{2-3}
      & Purchase & - \\ \cline{2-3}
      & Checkout & - \\ \hline 
    \end{tabular}
    \caption{Performance counters tracked during DS2 analysis.}
    \label{table:dell_counters}
  \end{center}
\end{table}

\subsection{Research prototype configuration}

All the tests in this case study were performed using quartile-stddev classifier with outlier boundaries set to baseline median $\pm 1$ standard deviation\footnote{See Section \ref{ss:repo_classification} for more details on the used classification algorithms.}, if not specified otherwise. 

The analysis was performed with \emph{minsup} changing between 10\% and 20\% and \emph{mindiff} changing between 30\% and 50\%\footnote{\emph{Minsup} and \emph{mindiff} are explained in section \ref{ss:arm_definition} and section \ref{ss:dasha_support-comparison} respectively.}.

\subsection{Additional analysis metrics}

The Dasha algorithm presented in Chapter \ref{cha:dasha} analyzes the differences between the two datasets and reports the most important violations that are found during the analysis. This algorithm does not assess the severity and nature of the reported violations. However, it is obvious that these violations may be of different importance. Let us consider two situations that would illustrate this difference between the violations that can be reported by the Dasha algorithm.

As a first example, let us assume that \emph{CPU system load} counter was tracked in two performance test runs and during their analysis the algorithm reports a violation for this counter that has 20\% support in the baseline dataset and 18\% support in the target dataset. In the baseline, 60\% of the counter values are in \emph{2nd} category and 40\% are in \emph{3rd} category. In the target, 50\% of values are in \emph{2nd} category and 50\% are in \emph{3rd} category.

As a second example, let us assume that during the same runs the algorithm also tracked \emph{MySQL received bytes} counter and after the analysis it reports a violation for this counter that has 100\% support in the baseline dataset and 90\% in the target dataset. In the baseline, 80\% of values are in \emph{2nd} category, 10\% are in \emph{1st} category and 10\% are in \emph{3rd} category. In the target, 10\% of values are in \emph{3rd} category and 90\% are in \emph{High} category.

It is obvious that the second violation is more severe and more attention has to be paid to it. While the observed minor differences in \emph{CPU system load} counter values could have been caused by data fluctuations appearing because of the stochastic processes in the hardware and software environments, the differences in \emph{MySQL received bytes} counter behavior most probably represent a significant performance regression. This conclusion can be made because the violations for the second counter are observed throughout the whole duration of the second test run and distribution of values between the baseline and target runs differs dramatically: while the counter values in the baseline are grouped near the median, the target values are mostly outliers that significantly exceed the  baseline values. 

As it can be seen from the provided example, the violations reported by the algorithm may be of different importance and to be more effective, the application implementing it should additionally inform the performance analyst about those violations that require more attention and have to be investigated first of all.

To reflect the differences in importance of the reported violations, the presentation algorithm uses three metrics, namely \emph{support}, \emph{severity} and \emph{hybrid}. \emph{Support} equals to the support of a violated rule antecedent in the baseline dataset and represents the fraction of a tracked time period during which the violation is observed. For the examples discussed earlier, the value of this metric equals 20\% and 100\% respectively.

\emph{Severity} metric is designed to be a measure of difference in categories distribution.This metric is used in the Dasha algorithm to find the most important association rule per given consequent\footnote{See Section \ref{sss:dasha_assoc-rules-analysis} for more details.}. It is measured as \(\frac{\sum_{i=1}^{c}|c_i.bsl\_sup-c_i.tgt\_sup|}{r.bsl\_sup+r.tgt\_sup}\), where $r$ is a violating association rule, $c$ -- rule consequents. For the examples discussed earlier, the value of this metric equals to approximately 10\% and 90\%.

\emph{Hybrid} is a composite metric that equals to $severity + support/5$. The violations reported by the Dasha algorithm get sorted according to this metric before being shown to the analyst. Hybrid metric was designed to reflect the changes both in support and severity and address the fact that for each of the rules support value always changes between 100\% and 0\% while the severity metric usually has smaller range and thus has to have higher weight. The difference in metrics weights was determined empirically.

\section{\label{sec:dell_executed-tests}Experiments}

\subsection{Experiments description}

During the described case study, four performance test runs were completed and compared using different analysis parameters. Each test run was executed for 60 minutes. Initially, two performance tests were run one after another to form a baseline for the analysis of the following runs. These tests were run with no alterations of DS2 source code. The second of these two test runs was analyzed against the first one. The other test runs were analyzed against a baseline formed out of the two first test runs.

Before launching the third test run, the code responsible for the registration procedure was altered by adding a delay to it. Because of this delay, the latency of the respective request increased by approximately 25\%. After comparing this test run with the baseline it was expected that \emph{Register latency} would be reported as a violation. Also, the introduced idle period should have had an impact on the values of some other counters gathered at a target machine because of slightly decreased load. The fourth test run had a larger delay added to the same place. For this test run, the average latency of registration request was increased by approximately 35\%. The reasons behind introducing this test run are described in the following section.

\subsection{\label{sec:dell_results}Analysis of results}

\subsubsection{Comparison of baseline runs}

After the first two test runs with no DS2 code alterations were executed, they were added to the system and compared against each other to make sure the algorithm does not discover violations in them and to find proper threshold values for \emph{minsup} and \emph{mindiff} parameters. The first run was considered a baseline run, the second one was considered a target run. When executing the analysis with $minsup=20\%$ and $mindiff=50\%$, no performance violations were detected between the runs. After the analysis was run with $minsup=15\%$ and $mindiff=50\%$, two violations were detected. However, these violations were raised with very low values of severity (0.1 and 0.2 respectively). The reported counters were then inspected manually using the time-charts generated by the research prototype. The investigation did not reveal significant differences in the values. For this reason, it can be concluded that the violations were reported because of minor data fluctuations usual for the performance testing datasets and were not related to the performance regressions that could have been introduced in the second run.

When the analysis was run with $minsup=10\%$ and $mindiff=50\%$, eight violations in total were reported. The values of \emph{hybrid} metric in them were generally higher than in the previous experiment, but either \emph{support} or \emph{severity} were low. Manual investigation of the newly reported violations did not reveal significant differences between the datasets.

When the analysis was run with $minsup=20\%$ and $mindiff=40\%$, four minor violations were reported, and when \emph{mindiff} was set to 30\%, eight violations were reported. Decreasing \emph{mindiff} to 20\% lead to reporting the same eight violations with slightly changed support and severity values. Experiments with simultaneously decreasing \emph{minsup} and \emph{mindiff} did not lead to additional interesting observations.

The analysis results discussed in this section clearly indicate that the performance testing datasets may contain significant amount of noise that has to be taken into account during the analysis. To remove this noise, either \emph{minsup} and \emph{mindiff} parameters have to be set to rather high values --- between 10\% and 20\% for \emph{minsup} and between 30\% and 50\% for \emph{mindiff} --- or the violations with low support or severity metric values have to be removed from the analysis results before showing them to the analyst. If \emph{minsup} and \emph{mindiff} are set to lower values, the analysis algorithm will detect and report even minor differences between the datasets. This behavior can be useful for certain applications, but is harmful for performance analysis because of the noise contained in the analyzed data.

It can thus be said that \textbf{Hypothesis 1} is valid proven that the algorithm's settings are high enough to mitigate the noise from data.

\subsubsection{\label{ss:dell_register-regression}Performance degradation of registration request}

To assess the ability of the research prototype to detect performance regressions in performance testing datasets, a delay was added to the registration request as described in section \ref{sec:dell_executed-tests}. The results of a performance test run with the introduced regression were analyzed against two prior test runs considered as a single baseline.

The first analysis was conducted with $minsup=20\%$ and $mindiff=50\%$. These values were empirically determined when comparing the baseline runs as sufficient thresholds for mitigating noise in data. During the analysis with these parameters, eight performance violations were detected, with six of them having $severity > 0.2$. Out of all counters belonging to \emph{Latency} category, only \emph{Register latency} counter was flagged as violation; the other reported counters were CPU- and database-related. 

\emph{Register latency} counter was flagged with $severity=0.43$, $support=0.62$. Two MySQL-related counters were considered more important than this counter due to higher values of $hybrid$ metric, the other counters were reported as less important. 

\subsubsection{Effects of changing the classifier configuration}

While the reported counters themselves were in line with the expectations, it was not clear why MySQL-related counters were flagged with higher priority than the target counter \emph{Register latency}. To investigate it further, time-series charts for the target counter and \emph{MySQL sent bytes} with $severity=0.53$, $support=0.62$ were examined. These charts are provided in Figure \ref{fig:dell_ts-regression}.

\begin{figure}[p]
  \centering
  \subbottom[\emph{MySQL sent bytes}]{
    \centering
    \includegraphics[scale=0.5,angle=90]{6-discreteChartMysql.eps}
    \label{fig:dell_ts-regression-mysql}
  }
  \quad
  \subbottom[\emph{Register latency}]{
    \centering
    \includegraphics[scale=0.5,angle=90]{6-discreteChartRegister.eps}
    \label{fig:dell_ts-regression-register}
  }%
\caption{Time-series charts and boxplots for experiment \ref{ss:dell_register-regression}.}
\label{fig:dell_ts-regression}
\end{figure}

From the boxplots it is clear that the difference between values of \emph{Register latency} counter is greater than the difference between \emph{MySQL sent bytes} values. This difference can also be seen in the time-series charts. However, if inspecting the categories into which the values fall (the category boundaries are shown at time-series charts with the dashed lines), it can be seen that most of the values for \emph{Register latency} counter fall into the same \emph{2nd} category as baseline values while the values for \emph{MySQL sent bytes} mostly get into lower categories. The reason for that is the way the values are distributed by categories in the classifier. Sometimes the analysis may fail to detect the violations (especially minor ones) because the values of a violating counter will still fall into the same category as the baseline values. However, in this situation most probably the violation won't remain undetected because it will be raised for the related counter as it can be seen here. 

The distribution of categories for the counter values in this experiment is shown in Figure \ref{fig:dell_cd-regression}. From the provided charts it can be seen that for \emph{MySQL sent bytes} counter it resembles normal distribution with mean in \emph{2nd} category for the baseline and in \emph{1st} category for the target. For all the categories, the average difference in confidence between baseline and target is high, which leads to high $severity$ value. For \emph{Register latency} counter, the average difference in confidence is smaller and this leads to smaller $severity$. 

\begin{figure}[p]
  \centering
  \subbottom[\emph{MySQL sent bytes}]{
    \centering
    \includegraphics[width=\textwidth]{6-distributionChartMysql.eps}
    \label{fig:dell_cd-regression-mysql}
  }
  \newline\newline
  \subbottom[\emph{Register latency}]{
    \centering
    \includegraphics[width=\textwidth]{6-distributionChartRegister.eps}
    \label{fig:dell_cd-regression-register}
  }%
\caption{Categories distribution for experiment \ref{ss:dell_register-regression}.}
\label{fig:dell_cd-regression}
\end{figure}

To mitigate the discussed problem, the configuration of a classifier can be adjusted. In the next experiment, the outlier boundaries were moved from $\pm 1$ standard deviation to $\pm 0.8$. After this change, the degradation of \emph{Register latency} was marked as the most important with $severity=0.55$, $support=1.0$. However, this change may lead to less precision for more volatile counters. It can be said that the parameters of the chosen classifier have to be found empirically based on the set of available performance counters. It could also be useful to allow the classifier to have different settings for the  different counters. However, this is not investigated further in this study and left for future work.

\subsubsection{\label{sss:dell-changing-configuration}Effects of changing the analysis configuration}

To understand the relation between the analysis settings and the results returned by the  performance degradations detection algorithm, the analysis of a run with degraded performance was repeated for several times with different parameters. Before these experiments, settings of the classifier were set to default values.

When the analysis was run with $minsup=0.15$, $mindiff=0.5$, all the important violation counters remained in place, three additional minor violations were reported. With $minsup=0.1$, $mindiff=0.5$, four minor violation were reported. After increasing \emph{minsup} gradually to 0.3 and 0.35 the algorithm was still able to detect the five most important violations. With $minsup=0.4$, one of the important violations (\emph{MySQL received bytes}) was not reported.

Several experiments were run with \emph{minsup} set to 0.2 to find the impact of changing \emph{mindiff}. Gradually decreasing it from 0.5 to 0.2, it was still possible to detect all the most important violations. However, the lower was \emph{mindiff}, the more minor violations were reported. Then, \emph{mindiff} value was gradually increased to 0.9. At this setting, \emph{Register latency} counter was not flagged as violating. However, at all the earlier steps with $mindiff=\{0.6,0.7,0.8\}$ the five most important violating counters were reported correctly. The higher was \emph{mindiff}, the higher was $severity$ and the lower was $support$ for \emph{Register latency} counter.

From the performed experiments it became clear that analysis settings $minsup=0.2$, $mindiff=0.5$ are the most suitable for detecting performance regressions in the described environment because with this settings the algorithm can efficiently filter out noise from the data and report the important violations.

\subsubsection{Another registration regression experiment}

As it was mentioned in section \ref{sec:dell_executed-tests}, performance testing data was also gathered for the test run with performance of registration request decreased by approximately 35\%. This test run was executed to see how the violations are reported when the values of the violating counter do not fall into the same category as the baseline values. 

This test was run with $minsup=0.2$ and $mindiff=0.5$. For this configuration, 11 violations were reported, all of them having medium or high severity. \emph{Register latency} counter, however, was reported as the most important, with $severity=0.97$, $support=1.0$. The next important counter was reported with $severity=0.67$, $support=0.62$. All five important violations that were reported when analyzing the previous test run with smaller delay introduced into the request processing code, were reported in this analysis as the most important. This behavior is perfectly correlated with the expectations.

\begin{center}
\line(1,0){250}
\end{center}

Thus, during the experiment \textbf{Hypothesis 2} was validated. After a regression was introduced into the new release of the target software, the research prototype flagged several performance counters as violations. It has to be noticed, however, that because of precision loss during the consolidation and classification as well as because of the noise contained in performance testing data, minor violations can be not detected by the implemented research prototype.

\section{\label{sec:dell_report-generation}Report generation}

In this section the presentation of performance tests analysis results is discussed. The presentation of data mining results used in the presented approach is similar to one suggested by Foo et al. in \cite{Foo10}. As an example here a sample output from comparing two performance test runs on Dell DVD Store benchmark is used. This section does not aim to thoroughly explain the used approach, as all the theoretical details behind the algorithm for detecting performance degradations are discussed in the previous chapters.

The analysis starts from test details page. It is shown in Figure \ref{fig:dell_test-details-page}. This page contains a list of test runs that are associated with the current test. For each test run in the list, its status (baseline or target) and execution time are shown. Also, it is possible to open full test run details by pressing a link with the test run name. This link leads to a performance repository page associated with the test run. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{6-testDetailsPage.eps}
  \caption{Test details page.}
  \label{fig:dell_test-details-page}
\end{figure}

The analysis is started by clicking ``analyze'' link. The selected target test run will then be compared against all the runs defined as a baseline. The results of the analysis are shown in Figure \ref{fig:dell_analysis-results}. This page contains a list of violating counters sorted by the value of $hybrid$ metric. The algorithm tries to mark the most important violations with red and yellow colors. 

Each of the violations can be expanded to provide the analyst with more details about itself. The expanded view contains links to the charts similar to the ones shown in Figures \ref{fig:dell_ts-regression} and \ref{fig:dell_cd-regression}, textual explanation of a violation and flagged association rule and a detailed table containing the details of categories distribution for baseline and target runs.

\begin{figure}[p]
  \centering
  \subbottom[\emph{Collapsed page}]{
    \centering
    \includegraphics[width=\textwidth]{6-analysisresultsCollapsedPage.eps}
    \label{fig:dell_analysis-results-collapsed}
  }
  \newline\newline
  \subbottom[\emph{Expanded page}]{
    \centering
    \includegraphics[width=\textwidth]{6-analysisResultsExpandedPage.eps}
    \label{fig:dell_analysis-results-expanded}
  }
\caption{Analysis results page.}
\label{fig:dell_analysis-results}
\end{figure}

\section{Summary}

The experiments performed by the author in this case study and while working on the research prototype implementation indicate that the Dasha algorithm presented in Chapter \ref{cha:dasha} can effectively compare the transaction databases and report the most notable differences using association rule mining techniques. The application of the algorithm for detecting performance regressions has shown that in order to be the most efficient, the algorithm's parameters should be adjusted to remove noise from data. Also, it is important to correctly set up a classifier distributing the continuous values of the tracked performance counters into discrete categories.

The experiments performed during this case study indicate that the algorithm is the most efficient when \emph{minsup} is set to 0.2, \emph{mindiff} is set to 0.5. Decreasing \emph{minsup} leads to polluting the results with the noise contained in the data; increasing it may lead to missing important violations. Changes in \emph{mindiff} have less impact on the returned results but if this setting is set to extremely low or high values, this may lead to incorrect results as well.

All the goals set before this case study were reached. The implemented research prototype was able to locate performance degradations introduced into Dell DS2 code. When comparing the baseline test runs against each other, no significant violations were reported. Report generation process was described.

Both the hypotheses were validated. It is important, however, to correctly configure the research prototype before the analysis to mitigate noise from data and do not lose important information contained in it.


