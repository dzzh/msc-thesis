\chapter{\label{cha:case_study_tomtom}Case study: TomTom PND Web Engineering}

In the previous chapter it was shown on a simple example how a research prototype implementing the Dasha algorithm can be used to automatically detect performance degradations between different test runs. The best combination of the algorithm's settings to remove noise from data and efficiently report performance violations was determined empirically.

In this chapter applicability of the research prototype in a real industrial setting is discussed. During his graduation project, the author worked at TomTom International BV on performance testing of a large e-commerce web application. As a part of his project, a set of performance tests covering main application use cases was developed. These tests were added to a continuous integration server for automated execution to test new application releases. The results of performance tests were analyzed using the research prototype presented earlier in this document. Description of these tests and findings made during their analysis are presented in this chapter. Because of a non-disclosure agreement, certain details about the system under study and its operation are omitted from this document.

\section{Web application under study}

\subsection{Application selection}

As it was covered in section \ref{sec:tomtom_we}, the main responsibility of TomTom PND Web Engineering department is developing and maintaining a set of web applications intended to support the company's clients who possess TomTom portable navigation devices and smartphone programs. These web applications are logically organized into a single ecosystem and communicate with each other by means of web requests. When deployed to production servers, the applications experience high load and it is vitally important for the company to ensure their correct operation in these conditions.

Because of a large number of system's responsibilities, its general complexity (both in terms of code and infrastructure, meaning support of several different testing environments, authentication and other security issues, etc.) and limited time frame dedicated for the case study, it was impossible to performance test the system as a whole. Instead, the tests were designed and performed against the Philadelphia web application, which is an important part of a system. This backend component has a number of important responsibilities and communicates both with the internal database and most of other system components. The Philadelphia database contains information about the digital products available for purchase in TomTom online store and customer-facing frontend applications fetch it via calls to the Philadelphia. Also, it implements a number of web services related to the management of digital content installed at the clients' devices.

Recently, this component was optimized for performance. After these optimizations, response times for most web requests served by it have decreased and became more stable and predictable than they were before the optimizations. However, certain requests processed by Philadelphia still remained unstable because of being dependent on the other components having less optimized code. It was requested by Philadelphia developers to prepare a set of performance tests and a scenario to execute them easily for assessing performance of the latest changes to the codebase as well as to ensure that no significant performance degradations are introduced between the application releases.

\subsection{Request groups}

Web requests served by Philadelphia can be logically divided into two groups. The requests from a first group interact only with the internal MySQL\footnote{\emph{http://mysql.com}} database that contains static data and is rarely updated. They read data from the database and transform it to a format requested by the user. However, these processing steps can be rather complicated and require a lot of computational power. This scenario allows to efficiently cache data at different levels of its processing and ensure stable response times under the required load. Despite the database is deployed separately from an application server, database queries do not introduce significant fluctuation of the request latencies, because MySQL is very well optimized and many calls to it are cached. Network latency also does not change much because all the servers are located nearby and are connected with high-bandwidth connections with excessive throughput.

The second group of requests depends on the other components of the system. Each request in this group triggers a number of additional requests to obtain necessary data from different system parts and cannot proceed until the responses to these requests are returned and processed. This makes performance testing a non-trivial task, because the resulting latencies obtained after generating the load and applying it to the system do not only contain information about the performance of a target component, but also include the latencies of the internal system components. Thus latency of these requests becomes a composite metric that is difficult to use for estimating performance of a target system component on its own.

For performance testing purposes it is needed to know both end-to-end latency that is a fraction of time between issuing a request to the target component (in this case, Philadelphia) and receiving a response and latency of a target component that is end-to-end latency minus the latencies of the external components. End-to-end latency is important for the end users as it represents a delay the user observes when issuing a request and awaiting for a response. Target component latency is important for the developers of this component, as it shows whether the main source of delays is in the target code or not.

It is clear that end-to-end latency and target latency are the same for requests from the first group, as they do not depend upon external components. Their performance testing can be done using Apache JMeter or a similar workload generator. For the requests from the second group the situation is different. It is impossible to get target latencies for the involved components of a system under study during performance testing with a workload generator only. There are two ways to get this information.

One of the possible approaches is to use a low-overhead profiler available in a number of contemporary application performance management suites, like AppDynamics\footnote{\emph{http://appdynamics.com}}. Such profilers can be used even at highly loaded servers and are able to display accurate timings of the executed requests at target components. A drawback of this approach is that these tools are rather expensive, hence they cannot be applied in all situations. Also, the existing products in this market are not yet flexible enough to provide raw data from their measurements, its extraction requires significant effort, if possible at all.

Another way to measure the latencies at the target component of a system under study is to mock the external components with stubs. During the described case study, this approach was used. If using stubs, every request to an external system gets intercepted by mocking code that immediately returns fake data generated according to the specified templates. One drawback of this approach is a need to program stubs in advance and make sure that the fake objects resemble real data. Another drawback is that mocking can only give approximate results. This happens because of difference in data and processor time distribution for the case when some of the external components are deployed together with the target component. However, as at TomTom environments all the system components are deployed separately, this issue was not taken into account and performance testing of these requests was done using already available mocks for the external parts of a system.

\section{Testing setup}

\subsection{Goals and hypotheses}

\paragraph{Goal 1} To study the behavior of the implemented research prototype on testing changes in performance of a large enterprise web application between two its releases.

\paragraph{Goal 2} To study the effects of changing the algorithm's settings on the returned results.

\paragraph{Hypothesis 1} The results of automated analysis using the research prototype will correlate with the results of manual analysis using time-series charts and aggregated values of the tracked performance counters.

\subsection{Hardware and software}

Similarly to the Dell DS2 case study described in the previous chapter, hardware setup for this case study consisted of three computers with the first of them hosting the target application, the second one being used as a load generator and the third one running OpenTSDB server for gathering hardware-related performance counters. The same performance counters as described in Table \ref{table:dell_counters} were captured in this study. Naturally, instead of DS2 request latencies, latencies of requests to the Philadelphia system were measured. The tests were executed with 50 threads instead of 10 threads used during Dell DS2 testing. In all the test runs \emph{minsup} was set to 0.2, \emph{mindiff} was set to 0.5, if not indicated otherwise.

All the performance tests written to analyze behavior of Philadelphia under different conditions were organized in a separate project and put under version control in the company's repository. The performance tests themselves were created using Apache JMeter\footnote{\emph{http://jmeter.apache.org}}. 

To automate tests execution, a scenario for their launch was prepared using Apache Maven\footnote{\emph{http://maven.apache.org}}. This tool is used to organize build processes for the evolving software projects. Its rich core functionality and a wide collection of available plugins allows to manage execution even of very complex build scenarios needed for large industrial projects. 

For letting Maven run JMeter tests and analyze their results, two plugins were set up, namely \emph{jmeter-maven-plugin}\footnote{\emph{https://github.com/Ronnie76er/jmeter-maven-plugin}} and \emph{jmeter-analysis-maven-plugin}\footnote{\emph{https://github.com/afranken/jmeter-analysis-maven-plugin}}. The first plugin allows to specify a directory where the plugins are kept and define properties that can later be read from JMeter scripts. Also, it runs the JMeter executable in the specified build phase. The second plugin helps to generate reports out of raw XMLs with JMeter performance testing results. To allow performance testing against different testing environments, separate profiles were created for each of them. 

To speed up information retrievals, caching is used pretty heavily in the Philadelphia. Hence, performance testing results may significantly differ between two test runs executed against the same software version if the state of caches is different prior to the tests' execution. To mitigate this problem, cache clearing requests were added at the initial step of each testing script.

One of requirements for the performance testing project was to set up automatic execution of performance tests on schedule. To satisfy this requirement, a respective job was added for pre-production testing environment to Jenkins continuous integration server\footnote{\emph{http://jenkins-ci.org}} used in the company. As the performance testing project created for this case study was a simple Maven wrapper over JMeter test files, it was rather easy to integrate it with default Jenkins installation. To support analysis of JMeter results and displaying performance trend charts from Jenkins, \emph{Performance} plugin\footnote{\emph{https://wiki.jenkins-ci.org/display/JENKINS/Performance+Plugin}} was added to it. This plugin cannot show details of the performance test runs or even distinguish different calls in JMeter tests, thus its applicability is rather limited. However, it is suitable for the initial estimation of application performance.

\section{Experiments}

\subsection{Experiments description}

Performance tests created to analyze Philadelphia performance were built to cover the requests from the both groups discussed above. However, no significant differences in test results behavior were found between the two groups, thus only the tests covering service \emph{S} that belongs to the first group of requests are discussed here. This service does not communicate with any of the external components. When it receives a request from a client, its processing steps to generate a response consist only from communicating with MySQL database and doing the necessary internal calculations and data transformations.

When Philadelphia is deployed at production servers or at one of the available testing environments, it is set up to cache the calls to this service for better performance. In the tests described in this chapter, service \emph{S} was tested with the caches being disabled and enabled in different testing configurations. These tests are analyzed separately.

Two Philadelphia releases were used in this case study. Release $R_{new}$ was the most recent one by the time the performance tests were run. It was considered a target release. Version $R_{old}$ was released approximately two months before $R_{new}$ and was treated as a baseline release.

All the tests performed in this case study were set up to run for approximately an hour. This duration of a run was chosen to gather a sufficient number of observations for the performance counters. At least five test runs were executed per each test configuration to ensure their stable behavior and discard the runs with significant performance fluctuations.

\subsection{Analysis of results}

\subsubsection{Discarding broken runs and mitigating effects of data fluctuations}

Before comparing the runs executed on $R_{old}$ version against $R_{new}$ version, the runs testing the same software version were compared against each other to make sure no significant violations are found between them. 

Analysis of baseline runs has shown that because of minor data fluctuations some counters are classified differently between the similar test runs and thus have different categories distribution even if comparing two executions that were run one right after another. Due to the stochastic structure of processes in non-real-time operating systems, background tasks and slightly different behavior of a load generator the aggregate performance counter metrics may differ between two runs by 0.5--1\%. This difference is usually difficult to see if just comparing the time-series for the respective runs. However, this may become a problem for a classifier, as values for many performance counters do not fluctuate significantly and are usually distributed very close to the median value. If in a target test run a median differs from the baseline by as little as 0.5\%, this difference becomes sufficient for the algorithm to classify the values of the respective counter differently. 

To mitigate this problem, it is needed to form a baseline out of several different test runs. In this case, the classifier would operate with more data and the categories would be a little wider. 

Also, it turned out that some test runs may contain counters that behave very differently from the same counters captured during the other runs. This issue may be caused by the background processes at a target machine or slightly changing performance of a load generator or network. To prevent polluting the results with these obviously wrong data, broken datasets were discarded before the analysis.

\subsubsection{Effect of slow calls to latency counters classification}

Testing Philadelphia application with enabled caches has uncovered an issue that was not observed during the earlier experiments. It turned out that the latencies are sometimes not consolidated correctly because of the slow calls. Most of the calls to the systems that use caching are completed extremely fast. At the same time, there almost always are cache misses, and the requests not fetching data from caches can take orders of magnitude longer to complete, when compared to the cached requests. It is especially easy to notice during the \emph{cold start}, when the system is started with all the caches being empty and many requests fetch data using heavy calculations because of a high percentage of cache misses. However, even when the caches are mostly filled, there still can be requests than do not hit them. Even if the percentage of cache misses is low, the respective requests may still significantly affect the aggregated latencies, as their values are much higher than those for optimized requests.

As an example of such behavior, let us consider calls to service \emph{S} during one of the test runs. If its results are cached, it takes around 250 ms to complete a request to it. However, if cache misses happen during its execution, the requests may take between 2000 and 11000 ms. Because of these slow calls, median latency at this run was reported as 380 ms, average latency was around 1500 ms. Raw and consolidated values for this performance counter are shown in Figure \ref{fig:tomtom_slow-requests}.

From Figure \ref{fig:tomtom_slow-requests-raw} it is clear that despite the fact that most of the requests are rather fast (median is at 380 at box-and-whisker plot), for many requests it takes several seconds to complete. 

In Figure \ref{fig:tomtom_slow-requests-consolidated} it is shown how these slow requests affect distribution of values into categories. It was expected that the values after the cold start would get into \emph{2nd} category, but most of them were classified as belonging to \emph{3rd} category because both the classifier and consolidated values were affected by the slow requests. To overcome this issue it can be recommended to use different classifiers for different performance counters and design a classifier taking this behavior into account for the latency counters. This is more flexible than having one classifier for all the counters, no matter what they are. However, this improvement was not implemented in the scope of this project.

\begin{figure}[ht]
  \centering
  \subbottom[\emph{Raw values}]{
    \centering
    \includegraphics[width=\textwidth]{7-slow-raw.eps}
    \label{fig:tomtom_slow-requests-raw}
  }
  \newline\newline
  \subbottom[\emph{Consolidated values}]{
    \centering
    \includegraphics[width=\textwidth]{7-slow-consolidated.eps}
    \label{fig:tomtom_slow-requests-consolidated}
  }%
\caption{Slow requests affecting latency counter.}
\label{fig:tomtom_slow-requests}
\end{figure}

\subsubsection{Analyzing service \emph{S} with disabled caching}

This experiment can be considered artificial, as caching is always enabled at the production servers. It was however interesting to see how its results correlate with the results obtained when testing the system with enabled caches.

The results of performance testing have indicated that most analyzed counters behave similarly in baseline and target runs. Thus, the median values for all but two performance counters in the target runs were fluctuating within 1\% from the respective baseline values. The differences were spotted in behavior of \emph{CPU context switches} and \emph{CPU System} counters. The former counter indicates how often the CPU switches between different processes or threads within a process. In all target runs the median values for this counter were approximately 6--7\% higher than in the baseline runs. The association rules reporting this violation were fired with $severity=0.56-0.62$ and $support=67-81\%$ during the analysis, which allows to consider this violation rather important. Median values for \emph{CPU System} counter in the target runs were approximately $2-4\%$ lower than in the baseline. This violation was detected with $severity=0.43-0.51$ and $support=43-81\%$. Most probably, these two violations are related, as if a number of context switches increases, CPU spends more time on loading and unloading data to/from registers that is reflected in \emph{CPU System} time. It is expected that \emph{CPU System} performance was also lower in the other target runs, but the algorithm was not able to distinguish \emph{CPU System} performance degradation from the noise.

Philadelphia developers have assumed that a cause of this degradation is an increased number of objects that are shared between different threads. Between $R_{old}$ and $R_{new}$ releases there were changes to the code base that could have led to such increase.

\subsubsection{\label{ss:tomtom_s_cached}Analyzing service \emph{S} with enabled caching}

Manual analysis using the aggregated values reported by the performance repository has shown that the most significant difference in performance between $R_{old}$ and $R_{new}$ is in behavior of \emph{CPU system} counter, as its median value has decreased by approximately 3--4\% in target releases. Average latency of one of two tracked requests $R1$ has improved by approximately 2\% in comparison with the baseline runs. The other counters were fluctuating within 1\% of baseline values.

Analysis of the target runs with the research prototype has shown that it is able to correctly detect these changes in performance. Thus, in all six valid target runs \emph{CPU System} counter was marked as a violation with average $severity=0.28$ and $support=100\%$ in 5 out of 6 cases. If considering violations raised within all six runs, this counter was the most important violation.

Latency counter $R1$ was also reported in all of the target runs, though it had lower severity and support values. It was reported with average $severity=0.18$ and $support=64\%$ in 5 out of 6 cases.

In a half of the target runs, \emph{CPU user} and \emph{CPU context switches} were also reported as important violations. Severity for \emph{CPU user} was fluctuating between 0.51--0.61, support was fluctuating between 29--55\%. For \emph{CPU context switches}, severity was 0.54--0.67, support was 26--41\%. Further investigation has shown that in the respective runs these counters were deviating from the baseline more than in the others, thus they were not reported in the rest of the target runs.

A number of minor violations was reported in each target run. An average number of reported violations was 6.7.

\subsubsection{Effects of changing analysis configuration}

When studying the applicability of a developed research prototype for performance analysis tasks on Dell DS2 example, the optimal combination of the association rule mining algorithm's settings was determined empirically\footnote{See Section \ref{sss:dell-changing-configuration}.}. In this configuration, \emph{minsup} was set to 0.2, \emph{mindiff} was set to 0.5. The former tests in this case study were completed with this configuration. In this section the performance testing results after altering the association rule mining algorithm's settings are studied. This analysis was performed to justify the results obtained during Dell DS2 case study.

All the analyses discussed in this section were performed against the results of performance testing \emph{S} service with enabled caching. The results presented in section \ref{ss:tomtom_s_cached} are considered reference for this section.
 
After increasing \emph{minsup} to 0.3, four out of six target test runs were reported as not having any violations. In two remained test runs, the number of detected violations has decreased, their severity and support have been lowered. 

When \emph{minsup} was increased to 0.4, the number of violations in two remained test runs has decreased even more. In neither of test runs the most violating counter \emph{CPU System} was reported.

After \emph{minsup} was changed to 0.1, more minor violations were reported in a number of target runs. Violations of \emph{CPU System} counter were considered in general less important.

It can be said that the optimal value of \emph{minsup} equal to 0.2 that was determined empirically in section \ref{sec:dell_results} turned out to be the most suitable for this set of tests as well. If \emph{minsup} is set to a larger value, some important violations may be not shown in the results. If this setting is set to be lower than optimal, the data may become polluted with the noise. Same trends were detected after analyzing the results with \emph{minsup} equal to 0.25 and 0.15. However, if changing \emph{minsup} with a lower step, the difference between the results is less apparent.

After setting \emph{minsup} back to 0.2 and increasing \emph{mindiff} to 0.6, the results generally became more clear as in three out of target six test runs the number of reported violations has decreased. In two of them, only \emph{CPU System} and \emph{R1} were reported.

When \emph{mindiff} was increased further to 0.7, \emph{CPU System} counter was still reported in all six target runs. Moreover, in five out of six runs, either only this counter or its combination with \emph{R1} were reported. It can be said, however, that this value is too high to be recommended to use, as the other CPU-related violations that were missed from a report with this algorithm's configuration, were also important.

Decreasing \emph{mindiff} to 0.4 led to the appearance of several additional minor violations in a number of target test runs and a slight change in severity and support metrics for a few counters. Decreasing it further to 0.3 led to even more new violations. The existing violations were reported with slightly lower values of severity and support. These results also correlate with the results obtained when studying the effect of changing the algorithm's settings in section \ref{sec:dell_results}. It can be said that changing \emph{mindiff} has lower effect on the reported violations than changing \emph{minsup}, and \emph{mindiff} values in a range between 0.4--0.6 are optimal for the performance analysis tasks.

\section{Summary}

A research prototype implemented in the scope of this project was able to detect performance regressions in a large industrial web application. Adjusting the algorithm's settings allows to change the threshold for the importance of reported violations.

In the previous chapter, the optimal combination of the algorithm's settings was determined. It was claimed that using this configuration the algorithm is able to distinguish performance degradations from the noise contained in the performance testing datasets and report all the important violations. Analysis of changing the algorithm's settings on TomTom performance testing datasets has justified this claim.

While analyzing certain target runs there may appear violations that are not typical for the rest of the runs. This happens because of noise in data and can be prevented by either changing the algorithm's settings or combining the results of several target run into one. 

If the system under study uses caching, then the performance counters tracking the behavior of requests' latency may be consolidated incorrectly due to the effect of slow requests. This issue can be avoided by categorizing values of such counters using a different classification algorithm.