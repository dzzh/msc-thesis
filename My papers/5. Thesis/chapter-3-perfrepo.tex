\chapter{\label{cha:perfrepo}Performance testing repository structure and functionality}

The main goal of the graduation project performed by the author was to apply and extend existing statistical methods for automatic detection of performance degradations in web systems. The performance degradations in the evolving software appear over time and to analyze them it is required to track performance of target applications during certain time periods. For correct detection of performance degradations and estimation of their severity, it is needed first to find a performance baseline, i.e. such values of the performance counters that are considered normal for the scope of completed performance test runs. These values can usually be determined out of one or several test runs marked as reference by the performance analysts. After the baseline is found, it can be used to understand whether the results of a new test run are in line with the previous runs, and if not, how severe the performance degradations are.

Performance test results analysis still remains mostly a manual and elaborate process. A usual way to the performance evaluation of web applications does not involve a thorough data mining-based analysis of the previously completed test runs. Instead, the analysis usually consists merely of the visual comparison of the performance charts between the most recent and preceding test runs. While this approach is sufficient for the applications that do not depend significantly on moderate changes in performance during the software evolution process or have long development cycles, for the rapidly evolving software that has to operate under high load it can be insufficient. The engineering teams developing such software may benefit from introducing the automatic approaches to detect performance degradations. 

\section{Related software}

As it was covered in the literature study, not much work has been done on automating performance degradations detection process until recently. For this reason, there exist no performance repositories aimed to organize  historical performance data for further statistical analysis. 

To the best of our knowledge, the only well-known software allowing to store historical data from the completed performance test runs and compare the runs between themselves is Microsoft Visual Studio (Ultimate Edition)\footnote{\emph{http://www.microsoft.com/visualstudio/eng}}. This is an advanced programming environment that includes load testing and performance analysis functionality. The Visual Studio allows performance analysts to build and execute complex performance test plans, gather performance counter values from the computational nodes and analyze the results of the test runs. It supports creating a Load Test Results Repository\footnote{\emph{http://msdn.microsoft.com/en-us/library/vstudio/ms318550.aspx}} to import and export performance test results. However, it does not include any methods for automated performance degradations analysis. Despite it is possible to create useful performance reports containing comparisons of certain performance counters between the test runs using the Visual Studio, this software does not decide whether the application performance has decreased and how important the changes in performance are. The performance analysts have to do a lot of work analyzing the performance reports to draw the respective conclusions.

Performance test analysis functionality in the Visual Studio has many benefits and this software is successfully used by many software development companies. However, for a number of reasons it cannot be used as a framework for building the performance repository for the statistical analysis of performance test results. First of all, this is proprietary software with closed-source code, which makes it impossible to extend its functionality in the required way. Secondly, it only supports test data in the Visual Studio format; it is impossible to import the data that is gathered with other performance testing tools. Also, the Ultimate Edition of the Visual Studio, which is the only edition supporting performance test analysis, is extremely expensive and it does not make much sense to purchase it for its performance testing functionality only.

The idea of a performance repository used to store the performance test results and analyze them for building performance baselines and detecting performance degradations using statistical methods was initially introduced in a scientific paper by Foo et al. in \cite{Foo10}. In this paper, the authors have described mining a performance testing repository using association rules to reveal the performance degradations in the web systems. However, a working version of the performance repository was not provided. Also, the authors did not address many important details of its functionality. Thus, taking into account the importance of having a performance testing repository for maintaining efficient performance engineering practices, this graduation project involved building an open-source performance testing repository having the properties described in the following section.

\section{Repository characteristics}

%TODO open repository, fix footnote
For the reasons discussed earlier in this document, it was decided to develop an open-source implementation of a performance testing repository as a part of the graduation project\footnote{The implementation of a repository is available at \emph{https://bitbucket.org/dzzh/mining}.}. The aim of this software is to support management and analysis of performance test results. The performance testing repository was built to assist performance engineers with the following tasks.

\begin{itemize}
\item Get easy access to the results of different performance tests ever run in the company.
\item Convert the results of performance test runs from different performance assessment tools into a common format for further comparisons.
\item Unify the values of the performance counters obtained from different nodes within the computational environment.
\item Make initial estimates about performance of newly added test runs in comparison with the baseline results.
\end{itemize}

A provided repository's implementation satisfies the following non-functional requirements. 

\begin{itemize}
\item Open-source. The repository code was written using publicly-available open-source components and is opened for public changes.
\item Multiple data import formats. The repository supports import of performance counter values in a number of different formats. Its architecture supports easy addition of the new formats.
\item Web interface. The repository can be deployed online to let users access it from different places and support collaboration.
\end{itemize}

\section{Technological stack}

The performance repository was built in the Python\footnote{\emph{http://python.org}} programming language using the Django\footnote{\emph{http://djangoproject.com}} web framework and MySQL\footnote{\emph{http://mysql.com}} database. jQuery\footnote{\emph{http://jquery.com}} and Twitter Bootstrap\footnote{\emph{http://twitter.github.io/bootstrap/}} frameworks were used for front-end development. All the software used to create a performance repository is open-source, easily accessible and can be used without limitations in the industrial setting. The repository itself is a WSGI web application that can be deployed online using Apache\footnote{\emph{http://apache.org}} or other Python-compatible web server. 

\section{Repository structure and terminology} 

The performance repository operates with several different entity types. At the highest level of entities' hierarchy stands \emph{a performance environment}. The performance environment is considered to be a set of computational nodes that are logically organized to perform a single task, e.g. support a distributed web application.

Each environment consists of one or more \emph{computational nodes}. These nodes may serve different purposes, e.g. function as back-end, front-end or database servers. It is assumed that the performance tests executed against the environment influence all the nodes belonging to that environment and do not impact the nodes outside the environment.

It is also assumed that the performance engineers build performance tests and launch them to test  behavior of the environment at a whole. Most commonly, such performance tests consist of a series of web requests to the load balancer or directly to the computational nodes. The performance of each test run may be assessed by means of various performance counters that can be gathered using different software either at environment level (e.g. latency of the certain requests) or at node level (e.g. CPU utilization of a back-end server or a number of database writes at a database server).

Each environment may contain a number of \emph{tests}. A test consists of a number of \emph{test runs}. It is assumed that each test contains the test runs that were launched with the same parameters against different versions of the target software. Separation of the performance tests from the test runs allows the performance engineers to run different performance tests on the same application and compare the results between the various runs with ease. The performance engineers may describe the test configuration in a test description.

The main entity of a performance testing repository is \emph{a test run}. Each run can be classified as either belonging to a baseline or target set. If a performance analyst classifies a test run as a baseline run, the values of the counters gathered during this test run are used together with the other baseline test runs to calculate a single baseline for estimating performance of the target runs. If a run is classified as belonging to a target set, its results are not used for the baseline calculations. After the performance analyst evaluates a new performance test run and does not find severe violations, he may change its status from target to baseline. The software allows to limit a number of baseline test runs used for the baseline calculations not to consider the results of testing legacy software. Limiting the number of baseline test runs may be useful after releasing a new version of software with significantly changed performance characteristics.

Each test run may include several different files with the performance counter values gathered during the test run. Their conversion to a common format is discussed in the following section.

The performance counters are defined separately and can be associated with a test run. One test run may have many different counters that are retrieved from different data files.

\section{Performance counters}

The performance counters are time series representing the results of monitoring certain components of a target system. In performance engineering they are used as measures of system performance. It can be said that all the performance engineering processes are directed to keep the values of the performance counters in line with expectations.

The goal of a performance repository is to store the performance counter values from different test runs to make them available for further analysis and comparisons. These data are retrieved from the external files created by third-party monitoring applications and is stored in the repository in two different formats.

First of all, the source data extracted from the files is stored as it is. To support this type of storage, so-called \emph{raw performance counters} are used. These counters consist of records, with each record used to store one value of a single counter with its associated timestamp and a label. The labels are used to distinguish the values coming from different computational nodes for node-related counters or the values belonging to different requests for environment-related counters. The raw counters are not used directly for data analysis, these activities are performed on the \emph{consolidated performance counters} that are created out of the raw counters.

The only difference between the data in the source files and its representation in a form of raw performance counters is the time frame. While the source files may contain arbitrarily large numbers of data points that are gathered during different overlapping time frames, the raw performance counters only keep a subset of data within the boundaries set by the performance analysts. It is needed to set the time boundaries for the test run start and end, because the accuracy of further analysis may suffer from inconsistent behavior of the performance counters during ramp-up and cool-down periods when not all the threads in a load generator are working.

Raw counter values are not used for data analysis directly because of two reasons. First of all, raw data has to be resampled. Such resampling is needed as typical input to a system may consist of many raw files gathered from different computational nodes using various monitoring software. Usually, the counters in these files are gathered with different sampling rates and even the same counters at different computational nodes have different timestamps due to the clock skew and stochastic structure of the processes execution in modern non-real-time operating systems. Thus, to correctly compare the different counters, all of them have to be down-sampled with the same sampling rate.

Secondly, the continuous structure of raw counter values does not allow to apply efficient data mining algorithms to derive meaningful information out of them. For these algorithms to work, the source data has to be classified in advance. Also, different performance counters have their values in different ranges and it is incorrect to compare the counter values based on this numerical and not-related data. Classification of counter values allows to mitigate this problem.

For these reasons, the repository supports the second type of storage. This storage operates with the entities called the \emph{consolidated performance counters} which are the main data source for the data analysis module. Consolidated counters are generated out of the raw counters using the consolidation process discussed below.

\subsection{Data consolidation process}

The goal of data consolidation process is to transform unrelated raw counter values into classified data. To achieve this, the data has first to be downsampled. The length of a sampling period should be set manually by a performance analyst and has to be larger than the lowest sampling rate for any of the counters. Say, for a test run lasting for an hour and containing the counter values gathered each 15 seconds or more frequently it makes sense to define a sampling period of 30 or 60 seconds. After the sampling period is set, raw data is split into the periods of this length. Each performance counter in a test run will then have as many values in its time series, as many periods of this length are in a test run time frame. All the values of a counter within each period are used to find a single value to be used in a new performance counter time series. To find this value, different strategies can be applied. The simplest ones are to use average or median values. As averaging is generally less accurate, median values are used for downsampling in the provided implementation of a performance testing repository.

Contrary to the raw performance counters, for the consolidated values there is no need to use timestamps to align the data points on a time scale. Instead, an offset is used. The first data point in a consolidated time series is assigned zero offset, for each consequent data point the offset increments. If for some of the sampling periods it happens that no respective raw counter values are available, a consolidated value is set to null, but is saved to the storage anyway.

After the raw counter values are downsampled, the median values with the associated offsets are stored in a database. Downsampled data don't get classified before being saved. The real classification is done on the fly right before the analysis. The reason behind this is that the classification depends not on the values in the same target test, but on the baseline values that change with the baseline runs set. For example, let's say the values of some performance counter in a target run are normally distributed between 10 and 20, and at a certain point in time a median value in the respective sampling period equals to 11. Then, in a system with \emph{Low}, \emph{Medium} and \emph{High} classification categories this value will most probably be classified as \emph{Low}, if the algorithm will rely on the values in the same target run only. If this category is fixed and saved in the database, it will only be known that at this point in time the counter had a relatively low value. This makes sense on its own, but this information will be meaningless if the results of a target run are compared with some baseline runs. Let's say that the baseline values of a respective counter are distributed between 5 and 13. In this case, 11 will most probably be classified as \emph{High}, not \emph{Low}. Thus, storing the median values instead of the categories is more flexible and leaves more information. The classification itself is performed at the analysis stage. Its details are discussed in the subsequent section.

If there exist several baseline test runs, to calculate baseline median and deviation their values are combined into a single array for each raw performance counter.

\subsection{\label{ss:repo_classification}Classification}

To convert the consolidated values of the performance counters into the categories that would be easy to analyze with data mining techniques, the performance testing repository uses classification modules (classifiers). Four interchangeable classifiers are available in the repository code. They are described in this section. 

All the classification algorithms available in the code of the performance repository rely on raw counter data to calculate the category boundaries. It would be easier to use downsampled data of the baseline tests for finding these aggregated values, but after experimenting with these data it turned out that downsampling introduces certain loss of precision that has to be taken into account. Hence, it was decided to rely on raw data for baseline calculations to make the classification more accurate. This means that for the classification both raw and discrete performance counters are needed.

\subsubsection{Median-based classifiers}

The first two classifiers were built to calculate the categories of consolidated performance counter values based on the medians and standard deviations of baseline raw counters. A taxonomy with three categories, namely \emph{Low}, \emph{Medium} and \emph{High}, is used in them. 

To find a category of a value in a performance test run, the classification algorithm first calculates median and standard deviation for the respective set of baseline raw counter values. Then, the downsampled value in a run is compared to the baseline median. If the first (\textbf{fixed-stddev}) classification method is chosen, the algorithm classifies the values that lie within the predefined number of baseline standard deviations from a median (1 by default) as \emph{Medium}, otherwise as \emph{Low} or \emph{High} depending on whether the target value is smaller or larger than the baseline median.

If the second classification method (\textbf{fixed-percentage}) is in use, the analyst has to specify a fixed percentage of values that are considered low or high in the baseline set. The algorithm then calculates the boundaries of a corridor allowed for the medium values based on this information and classifies the target data points as \emph{Medium} only if they make it in a corridor. Otherwise they are marked as \emph{Low} or \emph{High} the same way as in the previous method.

The experiments performed with these two counters have shown that they lack precision are are not well suitable for performance degradations detection. Thus, most of the baseline counter values get into \emph{Medium} category, yet only a small fraction of them is classified differently. The target values are classified as \emph{Low} or \emph{High} only if severe performance violations exist in the run, otherwise most of the values are also classified as \emph{Medium}. This makes it impossible to use these classifiers for detecting performance regressions that are not severe enough to be immediately detected during the manual analysis. To solve this problem, two more accurate classifiers were developed.

\subsubsection{Quartile-based classifiers}

These classifiers are more suitable for performance degradations detection as they operate with more categories and classify the values based on quartile points \cite{Watkins10}. Both these classifiers use a taxonomy with such categories as \emph{Low}, \emph{1st}, \emph{2nd}, \emph{3rd} and \emph{High}. \emph{Low} and \emph{High} categories are used to mark outlier values, \emph{1st} category is used for values that reside in the lower quartile ($Q_1$), \emph{3rd} category is used for values that belong to the upper quartile ($Q_3$) and \emph{2nd} category is used to classify the values lying in the interquartile range (i.e. between $Q_1$ and $Q_3$).
 
A simpler of two quartile-based classifiers is called \textbf{quartile}. It only uses the information about the quartile points to classify data values. To find outlier boundaries, it uses the smallest and largest values from the baseline. Thus, it is assumed that the baseline does not contain outlier values and they can only appear in the target. The second classifier is called \textbf{quartile-stddev} and uses median and standard deviation to calculate the boundaries for \emph{Low} and \emph{High} categories. At first, the classification algorithm used in this classifier calculates the median and standard deviation of the baseline values. Those values that do not get into the interval $median$ $\pm1$ $stddev$ are considered outliers and are classified as \emph{Low} and \emph{High} the same way as they are classified in \textbf{fixed-stddev} classifier. Then, the remained values (those that fall into \emph{Medium} category for \textbf{fixed-stddev} classifier) are classified using quartile-based classification the same way it is performed in \textbf{quartile} classifier. 

\begin{center}
\line(1,0){250}
\end{center}

The need to develop \textbf{quartile-stddev} classifier emerged because of the drawbacks of the classifiers developed beforehand. As it was mentioned earlier, median-based classifiers lacked precision in detecting relatively small performance degradations, as most baseline values were falling into a single category. A drawback of \textbf{quartile} classifier is that it assumes that the baseline does not have outlier values, that turned out to be a wrong assumption. \textbf{quartile-stddev} classifier properly distinguishes regular values from the outlier and provides three categories for the regular values thus having sufficient precision for performance engineering tasks.

The structure of a performance repository allows to easily add new classification algorithms that can better suit the needs of the performance analysts. The classification problem has many approaches and some of the available classification algorithms (for instance, those using the distribution properties of a baseline set) may show better results. However, the experiments performed during this research project and described in case studies (see Chapters \ref{cha:case_study_dell} and \ref{cha:case_study_tomtom}) indicate that \textbf{quartile-stddev} classifier works well in performance degradations detection tasks.

\section{Basic analysis tools}

The performance repository supports a simple technique for estimating the performance of a newly added test run. This technique is not as advanced as the methods implemented in the analysis module of the project, but it may also be helpful for estimating performance of the target runs.

If the performance of a new test run has decreased significantly, it can be easily detected either from the performance charts or by finding violation ratios for the available raw performance counters. The violation ratio here is defined as the total number of outliers (the values classified as \emph{Low} or \emph{High}) divided by the total number of samples. 

To help the analysts to easily detect broken test runs, each test run page contains a table listing all the available performance counters with such aggregate statistical metrics as minimum, maximum, average, median and standard deviation as well as their comparison with the respective metrics calculated over a baseline set. Also, the tables contain the violation ratios (separately for \emph{Low} and \emph{High} values). The table is sorted by the High violation column in descending order, which helps to overview the most severe problems in a run.

For the situations when the violation ratio is not very high but still suspicious, it may be useful to analyze the data by visually comparing the counter behavior in the target run with the baseline runs. To do this, the repository provides two different charts, namely a boxplot and a time-series plot. Both these charts are available for raw and discrete counters. It is recommended to use the charts based on the discrete counters for the analysis, but the raw charts can sometimes also be useful to check whether data consolidation leaded to significant loss of precision, which is an unlikely but possible case.

\section{Workflow}

\begin{wrapfigure}{r}{0.5\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{4-environments.eps}
  \end{center}
  \vspace{-20pt}
  \caption{List of environments.}
  \label{fig:repo_environments}
  \vspace{-10pt}
\end{wrapfigure}

This section describes what it looks like to work with the provided implementation of a performance testing repository from the prospective of a performance analyst. 

At first, the user has to define a testing environment. This step consists of setting its name and describing the computational nodes that form the environment and their roles in it. After this step, the user may have a look at the performance counters that are available in the system and add more counters if there is such a need. Then, the user should create a new test and specify its parameters in the description. This is needed for the situations when there exist many tests with slightly different parameters, i.e. different number of running threads or different state of caches. Also, the user has to set a length of a sampling period that will be the same for all the test runs.

\begin{wrapfigure}{l}{0.45\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.43\textwidth]{4-environment.eps}
  \end{center}
  \vspace{-20pt}
  \caption{Environment details.}
  \label{fig:repo_environment_details}
  \vspace{-20pt}
\end{wrapfigure}

When the test description and sampling period are set, the user has to launch his performance tests and gather the values of the performance counters. When these values are measured, the user should create a test run and set its start and end time as well as to specify whether the test run is used to form a baseline or has to be used as a target run.

After setting the test run parameters, the user may add data files containing the values of performance counters to it. After a test file is added into a system, the analyst chooses its format and selects the performance counters that can be found in the file. After all the preparations are done, the data extraction algorithm retrieves raw performance counters out of the test file and consolidates them. 

When raw and consolidated performance counters are created, they can be used by the analysis module. Also, the analyst may observe the statistical metrics and time-series charts at test run details pages to quickly detect the broken test runs. If certain counters were tracked incorrectly or their values significantly differ from the expected ones, they can be disabled for some time to let the analysis module compare the runs without considering these counters. 

The analyst adds as many performance test runs as it is needed. Each target run can later be compared with the previously defined baseline. This baseline is formed out of the runs that have satisfactory performance, as estimated by the analyst.

Some interfaces of a performance repository are shown at Figures \ref{fig:repo_environments} -- \ref{fig:repo_file_processing}. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{4-test.eps}
  \caption{Test details.}
  \label{fig:repo_test_details}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{4-testrun.eps}
  \caption{Test run details.}
  \label{fig:repo_testrun_details}
\end{figure}

\begin{figure}[ht]
  \centering
  \subbottom[\emph{Format selection}]{
    \centering
    \includegraphics[width=\textwidth]{4-file-format.eps}
    \label{fig:repo_file_format}
  }
  \newline\newline
  \subbottom[\emph{Counters selection}]{
    \centering
    \includegraphics[width=\textwidth]{4-file-counters.eps}
    \label{fig:repo_file_counters}
  }
\caption{Data files processing.}
\label{fig:repo_file_processing}
\end{figure}