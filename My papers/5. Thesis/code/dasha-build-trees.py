def support_trees(bsl_dataset, tgt_dataset, minsup, mindiff):
    #Decrease minimum support for target dataset
    tgt_minsup = minsup * (1 - mindiff)
    #Construct fp-trees
    bsl_fp_tree,tgt_fp_tree=fp_trees(bsl_dataset,tgt_dataset)
    #Generate frequent itemsets from fp-trees
    bsl_itemsets = freq_itemsets(bsl_fp_tree, minsup)
    tgt_itemsets = freq_itemsets(tgt_fp_tree, tgt_minsup)
    #Construct support trees
    bsl_support_tree = support_tree(bsl_itemsets)
    tgt_support_tree = support_tree(tgt_itemsets)
    return bsl_support_tree, tgt_support_tree

def fp_trees(bsl_dataset, tgt_dataset):
    #Implementation omitted, refer to FP-tree papers.
    bsl_fp_tree = fp_tree(bsl_dataset)
    tgt_fp_tree = fp_tree(tgt_dataset)
    return bsl_fp_tree, tgt_fp_tree

#Construction of frequent itemsets out of FP-tree. 
#Implementation omitted, can be found in FP-tree papers.
#Returns list of frequent itemsets.
def freq_itemsets(fp_tree,minsup):
    return itemsets

#Support tree generation
def support_tree(itemsets):
    #The tree is created following Definition 1 (Section 6.3)
    tree = SupportTree()
    for itemset in itemsets:
        tree.add(itemset)