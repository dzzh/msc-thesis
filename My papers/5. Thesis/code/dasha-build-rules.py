def build_rules(diff, mindiff,
                bsl_fp_tree, tgt_fp_tree)
    rules = list()
    for itemset in diff:
        for i = 0; i < itemset.length; i++:
            antecedent = copy(itemset)
            consequent = antecedent.pop(i)
            rule = rule(antecedent, consequent,
            			bsl_fp_tree, tgt_fp_tree,
            			mindiff)
            rules.add(rule)
	return rules

def rule(ant, con, bsl_fp_tree, tgt_fp_tree, mindiff): 
    #For better performance, FP-tree lookups should 
    #only be called if itemset is not found in support tree. 
    #For simplicity, search tree support is omitted.
    bsl_ant_sup = bsl_fp_tree.support_for(ant)
    bsl_con_sup = bsl_fp_tree.support_for(ant + con)
    tgt_ant_sup = tgt_fp_tree.support_for(ant)
    tgt_con_sup = tgt_fp_tree.support_for(ant + con)

	#No need to gen. rules if antecedents supports close
    if supports_close(bsl_ant_sup,tgt_ant_sup,mindiff):
        return None

    rule.antecedent = ant
    rule.consequent = con
    rule.bsl_support = bsl_con_sup
    rule.bsl_confidence = bsl_ant_sup/bsl_con_sup
    rule.tgt_support = tgt_con_sup
    rule.tgt_confidence = tgt_ant_sup/tgt_con_sup
	return rule

def fp_tree.support_for(itemset):
    itemset.fp_sort()
    paths = node_links(itemset.last)
    support = 0
    for path in paths:
        copied_itemset = copy(itemset)
        for item in path:
            if item in copied_itemset:
                copied_itemset.pop(item)
        if copied_itemset is empty:
            support += path.last.count
    return support