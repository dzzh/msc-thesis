\chapter{\label{cha:tomtom}Performance engineering practices at TomTom PND Web Engineering}

\section{Company overview}

TomTom International BV is a Dutch company manufacturing portable navigation devices and providing supplementary services to improve drivers' experience. The company has over 3500 employees located in nearly 40 offices all around the world. Its headquarters is in Amsterdam. 

A significant number of the company's employees are involved in designing, building, testing and delivering software, both for the mass market and internal use. These software include firmware for the manufactured devices, content updates (i.e. map changes), smartphone applications, web site and web shop, fleet management tools and more. 

It is natural that due to variability of applications developed by the TomTom engineers, software testing practices between the company's departments differ significantly. What is suitable for testing an embedded navigation system usually can hardly be applied for benchmarking a distributed web application serving customers all over the globe.

\section{\label{sec:tomtom_we}Web Engineering department}

In this chapter the performance engineering practices at TomTom Portable Navigation Devices Web Engineering (PND WE) department are described. PND WE is a part of Consumer group responsible for delivering products for the mass market. The main responsibility of the department is to develop and maintain web services oriented at the end users having TomTom PNDs or one of the navigation applications for the smartphones. The services developed at PND WE include management of customers' accounts and states of their associated devices, management of subscriptions to regularly updated content (maps, traffic, safety cameras and more), product catalog and order handling for the paid content, promotions activation, a dedicated web shop to let customers choose and download the content via web interface, common API layer to let PNDs connect to the services and more.

All these services interact with each other and are logically organized in a single system that is deployed at several dozens of computational nodes serving as front-end, back-end and database servers. Such a large number of the servers is needed as each component of the system is deployed at several machines and the requests to the systems are distributed between the nodes using the load balancers. Also, a number of servers is used to maintain legacy systems that are not actively developed any more but are still used by the customers possessing old navigation devices. Mutual dependence of the services from each other, different caching policies applied at the system's components and inconsistencies in the monitoring solutions supported by the different components make performance testing of the services in a system a non-trivial task. 

\section{Testing environments and release process}

\subsection{Development environments}

The company has a number of testing environments that aim to emulate the structure of the production system for testing purposes. These environments are built to serve different needs. First of all, there are five development environments to host unstable code coming from  early development branches. The most recent versions of the software run on these environments and the developers use them to perform functional tests addressing the core functions of the components and integration of new and updated features with the rest of the code base. Also, the developers usually connect to these environments from their local machines having one of the components deployed locally. While developing a particular part of a system, it is needed to establish communication with the other components and the development environments serve this purpose. These environments are small and compact, they have relatively poor performance which makes them less efficient for testing integration or performance issues, but they are perfectly suited for early functional testing and development support needs.

When all the feature branches of a system component pass initial functional tests at the development environments, the introduced changes get merged into a single release branch. As a general rule, after the initial merge of the feature branches, the release branch freezes from addition of the new features. It is only allowed to commit bug fixes into it. Release candidate code gets installed at one of the development environments. This is the starting point for the new release. After a successful deployment, a development environment with release candidate code is used by software engineers for alpha-testing.

\subsection{Early integration environment}

When the release branch passes the functional tests in a development environment, it receives a sign-off from a product owner and gets installed at an early integration environment for more thorough testing. This environment is used to host stable code builds that have already passed the initial testing procedures. While development environments are mostly used to test code changes within a certain component, the code deployed at the early integration environment is tested to ensure correct inter-component communication. 

The early integration environment is actively used by quality assurance engineers (QA). While the tests at the development environments are mostly run by the software developers whose main responsibility is to write program code, integration and regression testing is performed by the QAs who have more advanced tools, run more tests and verify the correctness of the deployed code at a higher level. Apart from using the automated tools to run different software testing scenarios, they also have a wide range of portable navigation devices produced by TomTom to test interaction with the system from the prospective of real users.

Another purpose of the early integration environment is to serve as an initial site for non-functional testing. Its logical structure resembles the structure of the production environment which allows the engineers to test deployment procedures on the release candidate build and verify the correct work of monitoring solutions. However, as the physical structure of the early integration environment does not look alike to the production system, it is not suitable for performance testing.

\subsection{Pre-production environment}

After the release branches of system components pass integration and regression testing stages at the early integration environment, they can be deployed at a pre-production environment for the final validation of deployment procedures and performance testing. The pre-production environment differs from the early integration environment as it resembles not only the logical structure of the system, but also its physical structure: all the components of the system are deployed at several physical computational nodes, communicate with several replicated database servers and are located behind a load balancer and a firewall. While the number of computational nodes differ at production and pre-production environments, the deployment procedures for them are exactly the same which means that once the release candidate version is successfully deployed to the pre-production environment, it can also be deployed to the production system following the same procedure. Also, performance of the pre-production environment is comparable to the production system which allows performance engineers to run their tests at the pre-production system and use the test results as a baseline for estimating performance of a release at the production system.

The pre-production environment is also used to investigate causes of incidents that happen at production system. If network engineers report strange behavior of the deployed software (e.g. unexpected errors, cache misses, customer complaints about the wrong data) and the cause of an incident is unclear to the developers after inspecting the logs, the pre-production environment gets synchronized with the production system and the engineers repeat failing scenarios at the pre-production system to locate the code causing the incident. In certain situations it is impossible to use lower-level environments for this tasks, as the components there are not distributed across several nodes. Some errors, however, appear only at the distributed environments and are caused not only by the flaws in the software, but also by its misconfiguration, state of the caches, infrastructural problems or hardware failures.

Only after the new release of a system component passes the final tests at the pre-production environment, it can be deployed to the production environment. During the deployment process and after it the network engineers constantly monitor the state of the servers to immediately detect the errors and roll the release back for investigation if deployment fails.

\begin{center}
\line(1,0){250}
\end{center}

Thus, at TomTom PND WE there are three different layers of testing environments and the production environment that accepts requests from the customers. All the environments have different roles and are used at different stages of testing and release processes. Keeping a number of different testing environments allows the engineers to simultaneously test several versions of the software, effectively investigate the incidents at the production system not affecting its usual operations and maintain automated release procedures to easily deploy new versions of the software to the production servers and roll them back in case of sudden failures.

\section{Performance testing practices}

Performance testing at PND WE is a responsibility of the performance engineering team. Usually, the performance tests are run at the pre-production environment when a stable release candidate code is deployed there. The performance tests are written using the HP LoadRunner\footnote{\emph{http://www8.hp.com/us/en/software-solutions/software.html?compURI=1175451}} tool. 

During the performance testing process, the engineers issue a large number of requests to the system. These requests represent the activities of virtual users that complete the most common tasks the real users do. These tasks include login into the system, retrieving the device status and available updates, downloading the list of available products, purchasing and downloading the content, etc. 

After the performance tests are run, the engineers manually analyze the resulting charts looking for general performance levels and unexpected spikes. In most situations, the new results are in line with the previous ones, which means that no additional investigation has to be performed. If a performance test shows suspicious results, they are reported to the development teams.

The results of the test runs are accessible in the intranet. Any software developer may look at performance of the components he is responsible for. However, there is no possibility to automatically compare the results of the previous runs with the results of the most recent test. It is a responsibility of an engineer to define a performance baseline and decide whether the performance in the latest test has degraded or improved.

If the tests reveal performance degradations in a new software release, the developers analyze its execution log and use dedicated monitoring and profiling tools to find the causes of the regressions. Performance degradations detection process is not automated, in every case all the investigation is performed manually using well-known heuristics.

\section{Performance monitoring tools and techniques}

\subsection{AppDynamics}

The most advanced monitoring tool used to track the behavior of PND WE software deployed online is AppDynamics\footnote{\emph{http://www.appdynamics.com}}. This is a dynamic application performance management suite that allows to monitor and troubleshoot web applications in real-time. It can analyze all the existing API calls and gather statistics for each of them as well as track the hardware counters. AppDynamics can build a map of the whole network environment in a company and show the request flows between the different servers. Also, it is able to work as a real-time software profiler with low overhead for the application servers and as an advanced database monitor for the database servers. It has flexible automation, visualization and notification options. AppDynamics is a very powerful and intelligent monitoring application that suits virtually every need of software developers and network engineers.

Despite the fact that AppDynamics covers most of the use cases needed for the software monitoring and troubleshooting needs, a number of less advanced monitoring-related tools are also used at PND WE. These tools are needed as even such advanced software as AppDynamics has its drawbacks. First of all, this software is very expensive and its price depends on the number of installed licenses. Hence, for some of the servers, especially not used to support the applications serving real customers (e.g. at testing environments) it is more cost-effective to use cheaper solutions. Secondly, AppDynamics is the new software that is being developed very actively and it has been initially deployed at PND WE just around a year ago. Its functionality and documentation change quite often and require significant time investments from the engineers who want to learn the most efficient ways to work with it. On the other hand, prior to AppDynamics deployment there were different monitoring solutions accepted at the department. They work well and changing all the custom scripts written to address the business needs for AppDynamics will not add any significant value. 

\subsection{Munin}

Thus, the Munin\footnote{\emph{http://munin-monitoring.org}} monitoring tool is still used widely for monitoring state of the application servers. This software is relatively old and it does not improve with the same pace as AppDynamics. It is a rather simple application that gathers different performance counters, stores them in a round-robin database with fixed amount of data\footnote{\emph{http://oss.oetiker.ch/rrdtool/tut/rrdtutorial.en.html}}, plots them and outputs the charts to a web page.

Munin has a number of serious drawbacks, as certain throughput issues or inability to gather the metrics with resolution less than five minutes. However, it has a large number of available plugins that allow to track many different counters with ease. Also, due to its simplicity it is very easy to build new plugins extending its functionality in whatever way needed. The existing Munin deployments at the department are supported with Nagios\footnote{\emph{http://nagios.org}} installations providing the alerting functional.

\subsection{OpenTSDB}

Another monitoring tool deployed at a number of TomTom servers and aimed to replace Munin for tracking and visualizing the performance counters is OpenTSDB\footnote{\emph{http://opentsdb.net}}. Contrary to Munin, this tool is built on top of Apache HBase\footnote{\emph{http://hbase.apache.org}} distributed database. This is a new application that was initially released just a year and a half ago. It does not have the drawbacks of Munin, as it scales well and allows to gather the counters with a second precision. Also, it does not override and merge the gathered data as Munin does for saving the disk space, thus OpenTSDB allows the engineers to get back to any gathered monitoring results for further analysis. However, this software is still not mature and its plotting capabilities are rather weak. Many improvements are still to be made to it before it becomes as easy to use as Munin is. It is also possible to write simple plugins for OpenTSDB, but as the software is new and its community is not yet large enough, not many ready-to-use plugins are available for immediate installation. For these reasons, many engineers still prefer Munin to OpenTSDB for monitoring the states of their servers.

\begin{center}
\line(1,0){250}
\end{center}

TomTom developers are currently working on a common monitoring infrastructure that would allow to write a single plugin tracking any important performance counters and to use its output in all the monitoring applications currently deployed at the department. However, switching to this infrastructure requires updating all the existing performance measuring scripts, which means this process will take a lot of time.

While AppDynamics tool is used both by software developers and network engineers, the other tools are mostly used by the network engineers only. As the software written in the department mostly does not have serious performance issues, there is no need to constantly monitor the state of the applications from the developers' side. However, any of the tools described earlier in this section can be used by the developers while investigating the performance-related incidents.

\section{Caching policies}

As the applications developed within the department serve millions of customers worldwide, they are usually used under high load. Thus, it is vitally important to correctly use caching to improve performance of the software and decrease the load in every way possible. For Java applications written at PND WE, Ehcache\footnote{\emph{http://ehcache.org}} is an adopted distributed caching framework. It is used both to store generated internal data between the similar requests and to keep the responses to SQL queries from an object-relational mapping framework. Until recently, every development team within the department had their own code responsible for interacting with the caching layer. Nowadays, the developers have switched to the common caching layer, which allows to decrease work on maintaining its code and to improve integration between different applications.

Apart from supporting caching at the applications level, certain caching policies are applied at the servers. These caches work on a higher level by processing the results of the requests to the company applications at a whole.

\section{Summary}

Performance is a serious concern for the engineers developing web applications at TomTom PND WE, as the software being created and maintained within the department serves millions of customers and operates under a high load. To ensure the quality of delivered code and the availability of the servers both during normal operation and in peak load condition, TomTom engineers apply different performance engineering practices, including benchmarking the applications at the testing environments, running various monitoring tools and observing their outputs, maintaining several software caching levels. Correct application of these practices allows to avoid most performance-related problems with the software being developed.


