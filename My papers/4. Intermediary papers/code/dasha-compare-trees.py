#Compare the trees and return the violating nodes
def compare_trees(bsl_tree, tgt_tree, mindiff):
    #Initialize comparison queue with root children
    queue = queue()
    for c in bsl_tree.root.children:
        queue.put(c)

    result = list()
    while queue:
        #Get a node from queue
        bsl_node = queue.pop()
        #Find correspondence in tgt (implementation omitted)
        tgt_node = find_node_in_tgt_tree(bsl_node)
        #Correspondence not found, report violation
        if not tgt_node:
            result.add(bsl_node.path)
        #Correspondence found
        else:
            sup_diff=(bsl_node_sup-tgt_node_sup)/bsl_node_sup
            #If nodes differ significantly, report violation
            if sup_diff > mindiff:
                result.add(bsl_node.path)
            #Otherwise add children of baseline node to queue
            else:
                queue.putall(bsl_node.children)

    #After the queue is empty, return violating paths
    return result