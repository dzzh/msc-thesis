def dasha_algorithm(bsl_dataset, tgt_dataset, minsup, mindiff):
	#See Listing 1.
    bsl_fp_tree,tgt_fp_tree = fp_trees(bsl_dataset,tgt_dataset)
    bsl_support_tree, tgt_support_tree = 
        support_trees(bsl_fp_tree, tgt_fp_tree, minsup)
  
  	#See Listing 2.
    diff = compare_trees(bsl_support_tree, tgt_support_tree, 
                         mindiff)

    #See Listing 4.
    rules = build_rules(diff, mindiff, 
                        bsl_fp_tree, tgt_fp_tree)
    
    #See Listing 5.
    return select_best_rules(rules, bsl_fp_tree, tgt_fp_tree)