#Additional processing of rules for their better presentation
def select_best_rules(rules, bsl_fp_tree, tgt_fp_tree):
    #Step 1: leave one rule for each different consequent
    best_rules = list()
    #Straightforward implementation omitted
    consequents = rules.get_all_different_consequents()
    for c in consequents:
        #Straightforward implementation omitted
        best_rules.add(rules.get_rule_with_max_severity(c))
        
    #Step 2: Add distribution of categories support
    result = list()
    for rule in best_rules:
        #Straightforward implementation omitted
        distr = get_categories_distr(rule, bsl_fp_tree, 
                                     tgt_fp_tree)
        result.add(distr)
    return result






    

