\documentclass[a4paper, notitlepage]{article}
\usepackage[utf8]{inputenc}
\begin{document}

\title{Performance repository structure and functionality}  
\author{Zmicier Zaleznicenka, MSc CS student, \#4134575}
\date{\today}
\maketitle

\setcounter{secnumdepth}{0}

The main goal of the graduation project performed by the author is to analyze and extend the existing statistical methods for automatic detection of performance degradations in web systems. The performance degradations in the evolving software appear over time and to analyze them it is required to track performance of the target applications during certain time periods. For the correct detection of the performance degradations and estimation of their severity, it is needed first to find a performance baseline, i.e. such values of the performance counters that are considered normal for the scope of the completed performance tests. These values can usually be determined out of one or several tests marked as reference by the performance analysts. After the baseline is found, it can be used to understand whether the results of the new test runs are in line with the previous runs, and if not, how severe the performance degradation is.

Performance test results analysis still remains mostly a manual and elaborate process. A usual way to the performance evaluation of web applications does not involve a thorough data mining-based analysis of the previously completed test runs. Instead, the analysis usually consists merely from the visual comparison of the performance charts between the most recent and preceding test runs. While this approach is sufficient for the applications that do not depend significantly on the moderate changes in performance during the software evolution process or have long development cycles, for the rapidly evolving software that has to operate under the high load this approach can be insufficient and the teams developing such software would benefit from introducing the automatic approaches to the detection of the performance degradations. 

\section{Related software}

As it was covered in the literature study, not much work has been done on automating performance degradations detection process until recently. For this reason, there exist no performance repositories aimed to organize  historical performance data for further statistical analysis. 

To the best of our knowledge, the only well-known software allowing to store the historical data from the completed performance test runs and compare the runs between themselves is Microsoft Visual Studio (Ultimate Edition)\footnote{http://www.microsoft.com/visualstudio/eng}. This is an advanced programming environment that includes load testing and performance analysis functionality. The Visual Studio allows the performance analysts to build and execute complex performance test plans, gather performance counter values from the computational nodes and analyze the results of the test runs. It supports creating a Load Test Results Repository\footnote{http://msdn.microsoft.com/en-us/library/vstudio/ms318550.aspx} to import and export performance test results. However, it does not include any methods for automated performance degradations analysis. Despite it is possible to create useful performance reports containing comparisons of certain performance counters between the test runs using the Visual Studio, this software does not decide whether the application performance has decreased and how important the changes in performance are. The performance analysts have to do a lot of work analyzing the performance reports to draw the respective conclusions.

Performance test analysis functionality in the Visual Studio has many benefits and this software is successfully used by many software development companies. However, for a number of reasons it cannot be used as a framework for building the performance repository for the statistical analysis of performance test results. First of all, this is a proprietary software with closed-source code, which makes it impossible to extend its functionality. Secondly, it only supports test data in the Visual Studio format, it is impossible to import the values of the performance counters retrieved using the different software packages. Also, the Ultimate Edition of the Visual Studio, which is the only edition supporting performance test analysis, is extremely expensive and it does not make much sense to purchase it for performance testing functionality only.

The idea of a performance repository used to store the performance test results and analyze them for building performance baselines and detecting performance degradations using statistical methods was introduced by Foo et al. in \cite{Foo}. In this paper, the authors have described mining a performance testing repository using association rules to reveal the performance degradations in the web systems. However, a working version of the performance repository was not provided. Also, the authors did not address many details of its functionality. Thus, taking into account the importance of having a performance testing repository for maintaining efficient performance engineering practices, this graduation project involved building an open-source performance testing repository having the properties described in the following section.

\section{Repository overview}

For the reasons discussed earlier in this document, it was decided to develop an open-source implementation of a performance testing repository as a part of the graduation project. The aim of this software is to support management and analysis of performance test results. The performance testing repository should assist the performance engineers with the following tasks.

\begin{itemize}
\item Get easy access to the results of different performance tests ever run in the company.
\item Convert the results of performance test runs from different performance assessment tools into unified format for further comparison.
\item Unify the values of performance counters obtained from different nodes within the computational environment.
\item Make initial estimates about performance of the new tests in comparison with the baseline results.
\end{itemize}

The repository is built to satisfy the following requirements. 

\begin{itemize}
\item Open-source. The repository code was written using publicly-available open-source components and is opened for public changes.
\item Multiple data import formats. The repository supports import of performance counter values in a number of different formats. Its architecture supports easy addition of the new formats.
\item Web interface. The repository can be deployed online to let users access it from different places and support collaboration.
\end{itemize}

\section{Technological stack}

The performance repository is built in Python\footnote{http://python.org} programming language using Django\footnote{http://djangoproject.com} web framework and MySQL\footnote{http://mysql.com} database. jQuery\footnote{http://jquery.com} and Twitter Bootstrap\footnote{http://twitter.github.io/bootstrap/} frameworks are used for front-end development. All the software used to create a performance repository is open-source, easily accessible and can be used without limitations in the industrial setting. The repository itself is a WSGI web application that can be deployed online using Apache\footnote{http://apache.org} or other Python-compatible web server. 

\section{Repository structure} 

The performance repository operates with several different entity types. At the highest level of entities' hierarchy stands \emph{a performance environment}. The performance environment is considered to be a set of computational nodes that are logically organized to perform a single task, e.g. support a distributed web application.

Each environment consists of one or more \emph{computational nodes}. These nodes may serve different purposes, e.g. function as back-end, front-end or database servers. It is assumed that the performance tests that are executed against the environment influence all the nodes belonging to this environment and do not have impact on the nodes outside the environment.

It is also assumed that the performance engineers create the performance tests and launch them to test the behavior of the environment at whole. Most commonly, such performance tests consist of a series of web requests to the load balancer or directly to the computational nodes. The performance of each test run may be assessed by means of various performance counters that can be gathered using different software either at environment level (i.e. latency of the certain requests) or at node level (i.e. CPU utilization of a back-end server or a number of database writes at a database server).

Each environment may contain a number of tests. \emph{A test} consists of a number of test runs. It is assumed that each test contains the test runs that were launched with the same parameters against the different versions of the target software. Separation of the performance tests from the test runs allows the performance engineers to run different performance tests on the same applications and compare the results between the various runs with ease. The performance engineers may describe the required test parameters in a test description field.

The main entity of a performance testing repository is \emph{a test run}. Each run can be classified as either belonging to a baseline or target set. If a performance analyst classifies a test run as a baseline run, the values of the counters gathered during this test run are used together with the other baseline test runs to calculate a baseline for estimating the performance of the target runs. If a run is classified as belonging to a target set, its results are not considered in baseline calculations. After the performance analyst evaluates a new performance test run and does not find severe violations, he may change its status from target to baseline. The software allows to limit a number of baseline test runs used for baseline calculations not to consider the results of testing legacy software. Limiting number of baseline test runs may be useful after releasing a new version of software with significantly improved performance characteristics.

Each test run may include several different files with the performance counter values gathered during the test run. Their conversion to the unified format is discussed in the following section.

The performance counters are defined separately and can be associated with a test run. One test run may have many different counters that are retrieved from different data files.

\section{Performance counters}

The performance counters are time series representing the results of monitoring the components of a target system. In performance engineering they are used as the measures of the system performance. It can be said that all the performance engineering processes are directed to keep the values of the performance counters in line with the expectations.

The goal of a performance repository is to store the performance counter values from the different test runs to make them available for the further analysis and comparison. These data is retrieved from the external files created by the third-party monitoring applications and is stored in the repository in two different formats.

First of all, the source data extracted from the files is stored as it is. To support this type of storage, so-called \emph{raw performance counters} are used. These counters consist of records, and every record is used to store one value of a single counter with its associated timestamp and a label. The labels are used to distinguish the values coming from different computational nodes for the node-related counters or the values belonging to different requests for the environment-related counters. The raw counters are not used directly for data analysis, these activities are performed on the adjusted counters that are created out of the raw counters.

The only difference between the data in the source files and its representation in a form of raw performance counters is the time frame. While the source files may contain arbitrary large numbers of data points that are gathered during different overlapping time frames, the raw performance counters only keep a subset of data within the boundaries set by the performance analysts. It is needed to set these boundaries for the test run start and end, because the accuracy of the further analysis may suffer from the inconsistent behavior of the performance counters during ramp-up and cool-down periods when not all the threads in a load generator are working.

Raw counter values are not used for data analysis directly due to a number of reasons. First of all, raw data has to be re-sampled. Re-sampling of the raw data is needed as typical input to a system may consist of many raw files gathered from different computational nodes using different monitoring software. Usually, the counters in these files are gathered with different sampling rates and even the same counters for the different machines have different timestamps due to the clock skew and the stochastic structure of the processes execution in modern non-real-time operating systems. Thus, to correctly compare the different counters, all of them have to be down-sampled with the same sampling rate.

Secondly, the continuous structure of the raw counter values does not allow to apply efficient data mining algorithms to derive meaningful information out of them. For these algorithms to work, the source data has to be classified in advance. Also, the different performance counters have their values in different ranges and it is incorrect to compare the counter values based on this numerical and not-related data. Classification of the counter values allows to mitigate this problem.

For these reasons, the repository supports the second type of storage. This storage operates with the entities called just \emph{performance counters} and they are the main data source for the data analysis module. Performance counters are generated out of the raw counters using data conversion process discussed below.

\subsection{Data conversion process}

The goal of the data conversion process is to transform unrelated raw counter values into classified data. To achieve this, the data has to be down-sampled. The length of a sampling period should be set manually by a performance analyst and has to be larger than the lowest sampling rate for any of the counters. Say, for a test run lasting for an hour and containing the counter values gathered each 15 seconds or more frequently it is meaningful to define a sampling period of 30 to 60 seconds. After the sampling period is set, raw data is split into the periods of this length. Each performance counter in a test run will have as many values in its time series, as many periods of this length are in a test run time frame. All the values of a counter within each period are used to find a single value to be used in a new performance counter time series. To find this value, different strategies can be applied. The simplest ones are to use average or median values. As averaging is generally less accurate, median values are used for down-sampling.

Contrary to the raw performance counters, for the converted values there is no need to use timestamps to align the data points on a time scale. Instead, an offset is used. The first data point in a converted time series is assigned zero offset, for each consequent data point the offset increments. If for some of the sampling periods it happens that no respective raw counter values are available, a converted value is set to null, but is saved to the storage anyway.

After the raw counter values are down-sampled, the median values with the associated offsets are stored in a database. Down-sampled data don't get classified before being saved. The real classification is done on the fly right before the analysis. The reason behind this decision is that the classification depends not on the values in the same target test, but on the baseline values that change with the baseline runs set. For example, let say that the values of some performance counter in a target run are normally distributed between 10 and 20, and at a certain point in time a median value in the respective sampling period equals to 11. Then, in a system with \emph{Low}, \emph{Medium} and \emph{High} classification categories this value will most probably be classified as Low, if rely on the values in the same target run only. If this category is fixed and saved in the database, it will only be known that at this point in time the counter had a relatively low value. This makes sense on its own, but this information will be meaningless if the results of a target run are compared with some baseline runs. Let say, that the baseline values of a respective counter are distributed between 5 and 13. In this case, 11 will be most probably classified as High, not Low. Thus, storing the median values instead of the categories is more flexible and leaves more information. The classification itself is performed at the analysis stage.

If there exist several baseline test runs, to calculate baseline median and deviation their values are combined into a single array for each raw performance counter.

\subsection{Classification}

The performance repository supports two slightly different classification methods. Both of them calculate the categories of the performance counter values based on the medians and standard deviations of baseline raw performance counters. Three categories, namely \emph{Low}, \emph{Medium} and \emph{High}, are used. 

To classify the value of a performance counter in a test run, the algorithm first calculates median and standard deviation for the respective set of baseline raw counter values. Then, the target value is compared to the baseline median. If the first (fixed-stddev) method is chosen, the algorithm classifies the values that lie within the predefined number of baseline standard deviations (1 by default) as \emph{Medium}, otherwise as \emph{Low} or \emph{High} depending on whether the target value is smaller or larger than the baseline median.

If the second classification method (fixed-percentage) is in use, the analyst has to specify a fixed percentage of values that are considered low or high in the baseline set. The algorithm then calculates the boundaries of a corridor allowed for the medium values based on this information and classifies the target data points as Medium only if they make it in a corridor. Otherwise they are marked as Low or High the same way as in the previous method.

The structure of a performance repository allows to easily add new classification algorithms that can better suit the needs of the performance analysts. Classification problem has many approaches and some of the available classification algorithms (for instance, those using the distribution properties of a baseline set) may show better results.

The existing classification algorithms rely on the raw counter data to calculate baseline medians and standard deviations. It would be easier to use the down-sampled data of the baseline tests for finding these aggregated values, but after experimenting with these data it turned out that the obtained results differ significantly from the raw results and it was decided not to use the down-sampled performance counters data for baseline calculations. This means that for accurate analysis both raw and normalized performance counters are needed.

\section{Essential analysis tools}

Despite all the advanced analysis methods are implemented in the analysis module of the project, the performance repository itself also supports a simple technique for estimating the performance of a newly added test run.

If the performance of a new test run has decreased significantly, it can be easily detected either from the performance charts or by finding violation ratios for the available raw performance counters. The violation ratio here is defined as total number of outliers (the values classified as \emph{Low} or \emph{High}) divided by the total number of samples. 

To help the analysts to easily detect the broken test runs, each test run page contains a table listing all the available performance counters with aggregate statistical metrics, as minimum, maximum, average, median and standard deviation values and their comparison with the respective values in a baseline set. Also, the tables contain the violation ratios (separately for Low and High values). The table is sorted by the High violation column in descending order, which helps to immediately see the most severe problems with the test run.

For the situations, when the violation ratio is not very high but still suspicious, it may be useful to analyze the data by looking at the chart comparing the counter behavior in the target run with the baseline runs. However, this functionality is left for future work.

\section{Workflow}

As the performance repository is still at an early development stage, its interfaces are not yet optimized for the ease of use, more effort is put on the ease of developing and testing the code. This section describes the suggested approach of working with the repository from the user's prospective. 

At first, the user has to define a testing environment. This step consists of describing the computational nodes that form the environment and their roles as well as setting the environment name. After this step, the user may have a look at the performance counters that are available in the system and add more counters if there is such a need. Then, the user creates a new test and specifies its parameters in the description. This is needed for the situations when there exist many tests with slightly different parameters, i.e. different number of running threads or different state of caches. Also, the user has to set a length of a sampling period that will be the same for all the test runs.

When the test description and sampling period are set, the user has to launch his performance tests and gather the values of the performance counters. When these values are ready, the user may create a new test run and set its start and end time as well as to specify whether the test run is used to form a baseline or has to be used as a target run.

After setting the test run parameters, the user may add the data files containing the values of performance counters to it. After the test files are added, each file has to be processed to retrieve raw performance counters out of it. To do this, the user has to choose a correct file format and specify the counters that can be found in the file. After converting the data from the source files to the raw performance counters, they can be converted to the performance counters. In the existing version of the repository this process is rather complicated and unclear. In future versions it can be simplified significantly. 

When both the raw performance counters and performance counters are created, they can be used by the analysis module. Also, the analysis may observe the statistical metrics at test run details pages to quickly detect the broken tests.

\begin{thebibliography}{9}
\bibitem{Foo} King Chun Foo, Zhen Ming Jiang, Bram Adams, Ahmed E. Hassan, Ying Zou, and Parminder Flora. Mining performance regression testing repositories for automated performance analysis. In \emph{Proc. 10th Int’l Conf. on Quality Software (QSIC’10)}, pages 32–41, Zhangjiajie, China, 2010.
\end{thebibliography}

\end{document}