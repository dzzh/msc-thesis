\documentclass[a4paper, notitlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{caption}
\usepackage{subcaption}

\graphicspath{ {images/} }

\begin{document}

\title{Case study: Dell DVD Store}  
\author{Zmicier Zaleznicenka, MSc CS student, \#4134575}
\date{\today}
\maketitle

Dell DVD Store\footnote{\emph{http://linux.dell.com/dvdstore/}} (DS2) is an open-source web benchmark that is often used by academic researchers and software developers in industry to performance test web software and server hardware. This is a rather simple web application that is intended to simulate a web store allowing its customers to browse and purchase DVDs. DS2 consists of a backend database component, a web application layer and driver programs. The underlying database component contains information about the registered users, available DVDs and fulfilled orders. During the setup process, it can be populated with automatically generated data of arbitrary size. Web application layer allows to browse the store and make purchases using a web browser. Driver program simulates the load to an application following the patterns common for web store customers.

The primary goal of this case study is to validate the automatic performance detection approach presented in the previous chapter on a relatively simple example. Another goal is to illustrate the workflow of a performance engineer during the analysis of the performance tests using the automatic performance regressions detection tool.

\section{Testing setup}

\subsection{Hardware and software}
The hardware setup for this case study consisted of three computers. Dell DS2 was installed at a dedicated Linux server with an automatically generated database of 20GB size. The database size was chosen to be several times larger than the amount of available RAM. This leaded to quite large read and write times both at database and disk system levels and resulted in clear performance testing results. A load generator was run from a laptop located in the same network as the target machine. The communication between two machines was set up using Wi-Fi connection. The values of the performance counters gathered at the target machine were stored in OpenTSDB time-series database\footnote{\emph{http://opentsdb.net}} instance that was deployed in a virtual machine in the cloud.

Dell DVD Store includes several different database backends and web layer implementations. For this case study, the application was configured to work with MySQL database and PHP web layer deployed at Apache web server. 

To have more control over the load generator and access the latencies of the issued requests, instead of using a default load driver supplied with DS2 benchmark, a performance test plan was created with Apache JMeter\footnote{\emph{http://jmeter.apache.org}} tool. The flow of this test plan resembled one of the default load driver and consisted of various user activities including login, registration, browsing and purchasing. All the performance test runs discussed in the chapter were run with 10 threads. Half of users issuing requests to DS2 were already registered and had to log in, the other half had to register first. Each user performed 5 different searches on average and each second user issued a request to buy some DVDs in the store after the searches.

\subsection{Tracked performance counters}

To analyze the performance of a target application, 17 performance counters were tracked and analyzed in the research prototype consisting of a performance testing repository\footnote{See Paper 2.} and an analysis module based on Dasha algorithm\footnote{See Paper 4.}. These counters are overviewed in Table \ref{table:counters}. Request latencies were gathered at a client machine with a load generator. Contrary to the values of the other counters, these values were gathered upon receiving a specific request. The other counters were gathered at a target machine with sampling periods of 15 and 60 seconds.

\begin{table}
  \begin{center}
    \begin{tabular}{ | c | c | c | }
  	  \hline
  	  \bf{Category} & \bf{Counter} & \bf{Sampling period, sec} \\
      \hline
      \multirow{5}{*}{CPU} & User load & 15 \\ \cline{2-3}
      & System load & 15 \\ \cline{2-3}
      & I/O wait & 15 \\ \cline{2-3}
      & Num interrupts & 15 \\ \cline{2-3}      
      & Num context switches & 15 \\ \hline
      \multirow{2}{*}{I/O system} & Read requests & 60 \\ \cline{2-3}
      & Write requests & 60 \\ \hline
      \multirow{2}{*}{MySQL} & Bytes received & 15 \\ \cline{2-3}
      & Bytes sent & 15 \\ \hline
      \multirow{7}{*}{Request latencies} & Browse actor & - \\ \cline{2-3}
      & Browse category & - \\ \cline{2-3}
      & Browse title & - \\ \cline{2-3}      
      & Login & - \\ \cline{2-3}
      & Register & - \\ \cline{2-3}
      & Purchase & - \\ \cline{2-3}
      & Checkout & - \\ \hline 
    \end{tabular}
    \caption{Performance counters tracked during DS2 analysis.}
    \label{table:counters}
  \end{center}
\end{table}

\subsection{Research prototype configuration}

All the tests in this case study were performed using quartile-stddev classifier with outlier boundaries set to baseline median $\pm 1$ standard deviation\footnote{Will be described in the performance repository paper. Basically, this is a classifier with five categories -- Low, 1st, 2nd, 3rd, High. At first, Low and High categories are calculated from outlier values, then outliers are removed from the results. After that, 1st category is used for the lowest 25\% of remained values, 2nd -- for values between 25\% and 75\%, 3rd for the highest 25\%.}, if not specified otherwise. 

The analysis was performed with $minsup$ changing between 10\% and 20\% and $mindiff$ changing between 30\% and 50\%\footnote{$minsup$ and $mindiff$ are explained in Paper 3 (Section 2.1) and Paper 4 (Section 4.2).}.

\section{Additional analysis metrics}

Dasha algorithm described in Paper 4 analyzes the differences between two datasets and reports the most important violations that are found during analysis. This algorithm does not assess the severity and nature of the reported violations. However, it is obvious that the violations may be of different importance. Let us consider two situations that would illustrate this difference between the violations that can be reported by Dasha algorithm.

As a first example, let us assume that \emph{CPU system load} counter was tracked in two performance test runs and during their analysis the algorithm reports a violation for this counter that has 20\% support in the baseline dataset and 18\% support in the target dataset. In the baseline, 60\% of the counter values are in \emph{2nd} category and 40\% are in \emph{3rd} category. In the target, 50\% of values are in \emph{2nd} category and 50\% are in \emph{3rd} category.

As a second example, let us assume that during the same runs the algorithm also tracked \emph{MySQL received bytes} counter and after the analysis it reports a violation for this counter that has 100\% support in the baseline dataset and 90\% in the target dataset. In the baseline, 80\% of values are in \emph{2nd} category, 10\% are in \emph{1st} category and 10\% are in \emph{3rd} category. In the target, 10\% of values are in \emph{3rd} category and 90\% are in \emph{High} category.

It is obvious that the second violation is more severe and more attention has to be paid to it. While the minor differences in \emph{CPU system load} counter values could have been caused by the data fluctuations appearing because of the stochastic processes in the hardware and software environments, the differences in \emph{MySQL received bytes} counter behavior most probably represent a significant performance regression. This conclusion can be made because the violations for the second counter are observed throughout the whole duration of test runs and distribution of values between the baseline and target runs differs dramatically: while the counter values in the baseline are grouped near the median, the target values are mostly outliers that significantly exceed the  baseline values. 

As it can be seen from the provided example, the violations reported by the algorithm may be of different importance and to be more effective, the application should additionally inform the performance analyst about those violations that require more attention and have to be investigated at first.

To reflect the differences in importance of the reported violations, the presentation algorithm uses three metrics, namely \emph{support}, \emph{severity} and \emph{hybrid}. \emph{Support} equals to the support of a violated rule antecedent in the baseline dataset and represents the fraction of a tracked time period during which the violation is observed. For the examples discussed earlier, this metric equals 20\% and 100\% respectively.

\emph{Severity} metric is designed to be a measure of difference in categories distribution. It is measured as \(\frac{\sum_{i=1}^{c}|c_i.bsl\_sup-c_i.tgt\_sup|}{r.bsl\_sup+r.tgt\_sup}\), where $r$ is a violating association rule, $c$ -- rule consequents. For the examples discussed earlier, this metric equals to approximately 10\% and 90\%.

\emph{Hybrid} is a composite metric that equals to $severity + support/5$. The violations reported by Dasha algorithm get sorted according to this metric before being shown to the analysts. Hybrid metric was designed to reflect the changes both in support and severity and address the fact that for each of the rules support value always changes between 100\% and 0\% while the severity metric usually has smaller range and thus has to have higher weight.

\section{Executed performance tests}
\label{sec:executed_tests}
During the described case study, four performance test runs were completed and compared using different analysis parameters. Each test run was executed for 60 minutes. Initially, two performance tests were run one after another to form a baseline for the analysis of the following runs. These tests were run with no alterations of DS2 source code. The second of these two test runs was analyzed against the first one. The other test runs were analyzed against a baseline formed out of the two first test runs.

Before launching the third test run, the code responsible for the registration procedure was altered by adding a delay to it. Because of this delay, latency of the respective request increased by approximately 25\%. After comparing this test run with the baseline it was expected that \emph{Register latency} would be reported as a violation. Also, the introduced idle period should have had an impact on the values of some other counters gathered at a target machine because of slightly decreased load. The fourth test run had a larger delay added to the same place. For this test run, the average latency of registration request was increased by approximately 35\%.

\section{Results}

\subsection{Comparison of baseline runs}

After the first two test runs with no DS2 code alterations were executed, they were added to the system and compared against each other to make sure the algorithm does not discover violations in them and to find proper threshold values for $minsup$ and $mindiff$ parameters. The first run was considered a baseline run, the second one was considered a target run. When performing the analysis with $minsup=20\%$ and $mindiff=50\%$, no performance violations were detected between the runs. After the analysis was run with $minsup=15\%$ and $mindiff=50\%$, two violations were detected. However, these violations were raised with very low values of severity (0.1 and 0.2 respectively). This allowed to make a hypothesis that these violations were caused by the data fluctuations, not due to existing performance regressions.

To validate this hypothesis, the values of the reported counters were inspected manually using the time-charts generated by the research prototype. The investigation did not reveal significant differences in the values that allowed to conclude that the violations were indeed reported because of minor data fluctuations usual for performance testing datasets.

When the analysis was run with $minsup=10\%$ and $mindiff=50\%$, eight violations in total were reported. The values of \emph{hybrid} metric in them were generally higher than in the previous experiment, but either \emph{support} or \emph{severity} were low. The manual investigation of the newly reported violations did not reveal significant differences existing in the datasets.

When the analysis was run with $minsup=20\%$ and $mindiff=40\%$, four minor violations were reported, and when $mindiff$ was set to 30\%, eight violations were reported. Decreasing $mindiff$ to 20\% leaded to reporting of the same eight violations with slightly changed support and severity values. Experiments with simultaneously decreasing $minsup$ and $mindiff$ did not lead to additional interesting observations.

The analysis results discussed in this section clearly indicate that the performance testing datasets may contain significant amount of noise that has to be taken into account during the analysis. To remove this noise, either $minsup$ and $mindiff$ parameters have to be set to rather high values --- between 10\% and 20\% for $minsup$ and between 30\% and 50\% for $mindiff$ --- or the violations with low support or severity metric values have to be removed from the analysis results before showing them to the analyst. If $minsup$ and $mindiff$ are set to lower values, the analysis algorithm will detect and report even minor differences between the datasets. This behavior can be useful for certain applications, but is harmful for performance analysis because of the noise contained in the analyzed data.

\subsection{Performance degradation of registration request}
\label{sec:register_regression}

To assess the ability of the research prototype to detect performance regressions in performance testing datasets, a delay was added to the registration request as described in section \ref{sec:executed_tests}. The results of a performance test run with the introduced regression were analyzed against two prior test runs considered as a single baseline.

The first experiment was conducted with $minsup=20\%$ and $mindiff=50\%$. These values were empirically determined during the experiments with comparing baseline runs as sufficient thresholds for mitigating noise in data. During the analysis with these parameters, eight performance violations were detected, six of them having $severity > 0.2$. Out of all counters belonging to \emph{Latency} category, only \emph{Register latency} counter was flagged as violaton; the other reported counters were CPU and database-related. 

\emph{Register latency} counter was flagged with $severity=0.43, support=0.62$. Two MySQL-related counters were considered more important than this counter due to higher values of $hybrid$ metric, the other counters were reported as less important. 

\subsubsection{Effects of changing classifier configuration}

While the reported counters themselves were in line with the expectations, it was not clear why MySQL-related counters were flagged with higher priority than the target counter \emph{Register latency}. To investigate it further, time-series charts for the target counter and \emph{MySQL sent bytes} with $severity=0.53, support=0.62$ were examined. These charts are provided at Figure \ref{fig:ts_regression}.

\begin{figure}[p]
\centering
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[scale=0.5,angle=90]{5-discreteChartMysql.png}
    \caption{\emph{MySQL sent bytes}}
    \label{fig:ts_regression_mysql}
  \end{subfigure}%
  ~
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[scale=0.5,angle=90]{5-discreteChartRegister.png}
    \caption{\emph{Register latency}}
    \label{fig:ts_regression_register}
  \end{subfigure}%
\caption{Time-series charts and boxplots for experiment \ref{sec:register_regression}.}
\label{fig:ts_regression}
\end{figure}

From the boxplot charts it is clear that the difference between values of \emph{Register latency} counter is greater than the difference between \emph{MySQL sent bytes} values. This difference can also be seen at the time-series charts. However, if inspecting the categories into which the values fall (the category boundaries are shown at time-series charts with the dashed lines), it can be seen that most of the values for \emph{Register latency} counter fall into the same \emph{2nd} category as baseline values while the values for \emph{MySQL sent bytes} mostly get into lower categories. The reason for that is the way the values are distributed by categories in the classifier. Sometimes the analysis may fail to detect the violations (especially small ones) because the values of a violating counter will still fall into the same category as the baseline values. However, in this situation most probably the violation won't remain undetected because it will be raised for the related counter as we see here. 

The distribution of categories for the counter values in this experiment is shown at Figure \ref{fig:cd_regression}. From the provided charts it can be seen that for \emph{MySQL sent bytes} counter it resembles normal distribution with mean in \emph{2nd} category for the baseline and in \emph{1st} category for the target. For all the categories, the average difference in confidence between baseline and target is high, which leads to high $severity$ value. For \emph{Register latency} counter, the average difference in confidence is smaller and this leads to smaller $severity$. 

\begin{figure}[p]
\centering
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[width=\textwidth]{5-distributionChartMysql.png}
    \caption{\emph{MySQL sent bytes}}
    \label{fig:cd_regression_mysql}
  \end{subfigure}%
  \newline\newline
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[width=\textwidth]{5-distributionChartRegister.png}
    \caption{\emph{Register latency}}
    \label{fig:cd_regression_register}
  \end{subfigure}%
\caption{Categories distribution for experiment \ref{sec:register_regression}.}
\label{fig:cd_regression}
\end{figure}

To mitigate the discussed problem, the configuration of a classifier can be adjusted. In the next experiment, the outlier boundaries were moved from $\pm 1$ standard deviation to $\pm 0.8$. After this change, the degradation of \emph{Register latency} was marked as the most important with $severity=0.55, support=1.0$. However, this change may lead to less precision for more volatile counters. It can be said that the parameters of the chosen classifier have to be found empirically based on the set of available performance counters. It could also be useful to allow the classifier to have different settings for the  different counters, however, this is not investigated further in this study.

\subsubsection{Effects of changing analysis configuration}

To understand the effect of analysis settings on the performance detection results, the experiment with detecting performance degradation of a registration request was repeated several times with different parameters. For all these experiments, settings of the classifier were set to default values.

When the analysis was run with $minsup=0.15, mindiff=0.5$, all the important violation counters remained in place, three additional minor violations were reported. With $minsup=0.1, mindiff=0.5$, one additional minor violation was reported. With $minsup=0.3$ and $minsup=0.35$ the algorithm was still able to detect five the most important violations. With $minsup=0.4$, one of the important violations (\emph{MySQL received bytes}) was not reported.

Several experiments were run with $minsup$ set to 0.2 to find the impact of changing $mindiff$. Gradually decreasing it from 0.5 to 0.2, we were able to detect all the most important violations. However, the lower was $mindiff$, the more minor violations were reported. Then, $mindiff$ value was gradually increased to 0.9. At this setting, \emph{Register latency} counter was not flagged as violating. However, at all the earlier steps with $mindiff=\{0.6,0.7,0.8\}$ five the most important violating counters were reported correctly. The higher was $mindiff$, the higher was $severity$ and the lower was $support$ for \emph{Register latency} counter.

From the performed experiments it became clear that analysis settings $minsup=0.2, mindiff=0.5$ are the most suitable for detecting performance regressions in the described environment because with this settings the algorithm can efficiently filter out noise from the data and report the important violations.

\subsubsection{Another registration regression test}

As it was mentioned in section \ref{sec:executed_tests}, performance testing data was also gathered for the test run with performance of registration request decreased by approximately 35\%. This test run was executed to see how the violations are reported when the values of the violating counter do not fall into the same category as the baseline values. 

This test was run with $minsup=0.2$ and $mindiff=0.5$. For this configuration, 11 violations were reported, all of them having medium or high severity. \emph{Register latency} counter, however, was reported as the most important, with $severity=0.97,support=1.0$. The next important counter was reported with $severity=0.67,support=0.62$. All five important violations that were reported when analyzing the previous test run with smaller delay introduced into the request processing code, were reported in this analysis as the most important. 

\section{Report generation}
\label{sec:report_generation}

In this section we discuss presentation of results generated by Dasha algorithm and performance tests analysis using association rules mining approach introduced in the previous chapters. The presentation of data mining results used in our approach is similar to the one suggested by Foo et al. in \cite{Foo}. As an example here we use an output from comparing two performance test runs on Dell DVD Store benchmark. This section does not aim to thoroughly explain the used approach, as all the theoretical details behind the algorithm for detecting performance degradations were discussed in previous chapters.

The analysis starts from the test details page. It is shown at Figure \ref{fig:test_details_page}. This page contains a list of test runs that are associated with the current test. For each test run in the list, its status (baseline or target) and execution time are shown. Also, it is possible to open full test run details by pressing a link with the test run name. This link directs to a performance repository page associated with the test run. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{5-testDetailsPage.png}
  \caption{Test details page.}
  \label{fig:test_details_page}
\end{figure}

The analysis is started by clicking 'analyze' link. The selected target test run will then be compared against all the runs defined as a baseline. The results of the analysis are shown at Figure \ref{fig:analysis_results}. This page contains a list of violating counters sorted by the value of $hybrid$ metric. The algorithm tries to mark the most important violations with red and yellow colors. 

Each of the violations can be expanded to provide the analyst with more details about it. The expanded view contains links to the chart similar to the ones shown at Figures \ref{fig:ts_regression} and \ref{fig:cd_regression}, textual explanation of a violation and flagged association rule and a detailed table containing the details of categories distribution for baseline and target runs.

\begin{figure}[p]
\centering
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[width=\textwidth]{5-analysisresultsCollapsedPage.png}
    \caption{\emph{Collapsed page}}
    \label{fig:analysis_results_collapsed}
  \end{subfigure}%
  \newline\newline
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[width=\textwidth]{5-analysisResultsExpandedPage.png}
    \caption{\emph{Expanded page}}
    \label{fig:analysis_results_expanded}
  \end{subfigure}%
\caption{Analysis results page.}
\label{fig:analysis_results}
\end{figure}

\section{Conclusions}

The experiments performed by the author in this case study and while working on the research prototype implementation indicate that Dasha algorithm presented in Paper 4\footnote{Proper x-ref will be made after merging papers into thesis doc.} can effectively compare the transactional databases and report the most notable differences using association rules mining techniques. The application of the algorithm for detecting performance regressions based on the analysis of the performance counters has shown that for the algorithm to be the most efficient, its parameters should be adjusted to remove noise from data. Also, it is important to correctly set up a classifier distributing the values into discrete categories.

\begin{thebibliography}{99}
\bibitem{Foo} King Chun Foo, Zhen Ming Jiang, Bram Adams, Ahmed E. Hassan, Ying Zou, and Parminder Flora. Mining performance regression testing repositories for automated performance analysis. In \emph{Proc. 10th Int’l Conf. on Quality Software (QSIC’10)}, pp. 32–41, Zhangjiajie, China, 2010.
\end{thebibliography}

\end{document}