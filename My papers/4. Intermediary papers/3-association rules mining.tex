\documentclass[a4paper, notitlepage]{article}
\usepackage[utf8]{inputenc}

\mathchardef\mhyphen="2D

\begin{document}

\title{Mining performance testing data \\with association rules and frequent itemsets}  
\author{Zmicier Zaleznicenka, MSc CS student, \#4134575}
\date{\today}
\maketitle

The main goal of a performance testing repository discussed in the previous chapter is to store the aggregated values of the performance counters. This should allow performance analysts to track the changes in performance of target applications during their lifecycles. Despite the raw performance data can be useful on its own and certain conclusions about the quality of a software package under study can be made after applying the simple analysis methods discussed earlier, there exist situations when performance degradations are not obvious and are difficult to detect with simple heuristics. To reveal the performance degradations in these cases, certain data mining algorithms can be used. In this chapter we will discuss the applicability of association rules mining algorithms to performance testing datasets. 

\section{Classification of data mining algorithms}

As we have pointed out earlier, the analysis of performance test results remains to a large extent manual and highly inefficient process, though its tasks may and have be automated. Many data analysis algorithms were developed recently and some of them may be efficient for the analysis of performance testing data stored in a performance testing repository. Deriving knowledge from patterns occurring in the large volumes of data is known as knowledge discovery in databases (KDD) and to find the algorithms suitable for mining performance testing data we have referred to this topic. KDD is an important and broad research field that has much attention from the research community and covers a rather large set of tasks nowadays. Fayyad et al. in \cite{Fayyad} divide the data mining tasks used in KDD process into six different classes, namely \emph{classification}, \emph{regression}, \emph{clustering}, \emph{summarization}, \emph{dependency modeling} and \emph{change and deviation detection}. Many of these classes cover the problems similar to ones to be solved while analyzing performance tests.

The main problem of mining performance testing datasets in the scope of the discussed project can be stated as finding the degree of similarity between datasets containing performance observations. The performance data that is stored in the repository either belongs to a baseline dataset or to a target dataset. To conclude whether a target run was successful or not, it is needed to compare the values of the performance counters gathered during the target run with the values of the respective baseline runs. If the values and their distributions are similar, the target run may be considered having satisfactory performance. If the baseline and target datasets differ significantly, this means that the target run has different performance and this difference has to be reported for further investigation. 

The degree of similarity between two datasets can be found in several different ways. First of all, it is possible to apply the \emph{classification} algorithms to mark each of the transactions in a target data set as successful or failed and make a conclusion about the run at a whole based on the percentage of failed transactions. Secondly, \emph{change and deviation detection} algorithms can be used to discover the most important changes in a target set. This information can later be used to determine whether this dataset differs significantly from its baseline or not. The third possible way of comparing the datasets is with \emph{dependency modeling} algorithms. These algorithms build a dependency model that describes significant dependencies between the variables. To find the degree of similarity between the datasets using dependency modeling, it is needed to derive a dependency model for a baseline set and apply it to the target dataset. If the target dataset fits the model, this means that it is similar to the baseline. Otherwise, baseline and target datasets have to be considered dissimilar.

\section{Association rules mining}

Due to the large amount of work needed to be done it is not feasible to implement and compare all the above-mentioned approaches within a scope of a single graduation project. Hence, this study addresses only the applicability of dependency modeling algorithms on mining performance testing data. Later in this document we will refer to dependency modeling as to \emph{association rules mining}. This term was introduced by Agrawal, Imielinski and Swami in \cite{Agrawal93} and is commonly used for referencing dependency modeling algorithms nowadays.

Association rules mining technique was introduced as a tool primarily aimed at market basket analysis. By early 1990s, improvements in barcode and data storage technologies made it possible for large retail chains to keep records of every combination of items ever purchased by the customers and update these records in near real-time. Processing of customers' transactions may help to find important patterns in their behavior that can later be used to improve the effectiveness of marketing campaigns, merchandise and sales planning. These activities eventually lead to higher profits for companies that employ data analysis techniques to find knowledge in their data. For this reason, a lot of attention was paid to market basket analysis and association rules mining in last two decades. Several important algorithms addressing the problem were developed and adopted for various scenarios. Apart from being used for market basket analysis, these algorithms are successfully applied in network security and  computational biology as well as for time series analysis \cite{Hegland}.

\subsection{Formal definition}

Association rules mining problem can be stated as follows \cite{Agrawal93}. There exist a set of items \(I = \{a_1,a_2,...,a_n\}\) and a set of transactions \(T = \{T_1,T_2,...,T_m\}\). Each transaction contains an itemset from $I$, i.e. \(T_i \subseteq I\). An itemset containing $k$ items is called $k{\mhyphen}itemset$. An association rule is defined as an expression \(X \Rightarrow Y\) where $X$ and $Y$ are also itemsets from $I$. This expression means that if \(X \in T_i\), than probably \(Y \in T_i\). The rule \(X \Rightarrow Y\) has support $s$ if $s$\% of transactions in $T$ contain \(X \cup Y\). The rule holds with confidence $c$ if $c$\% of transactions in $T$ that contain $X$ also contain $Y$.

Having a transaction database $T$\footnote{For association rules mining to work, underlying datasets may be presented in any machine-readable format. In this document we assume that the transactions are logically organized in a form of a single-table SQL database.}, the problem of mining association rules is to find all the rules having support and confidence higher than the user-defined thresholds. These thresholds are called minimum support (\emph{minsup}) and minimum confidence (\emph{minconf}). The itemsets having at least minimum support are called \emph{large} or \emph{frequent}.

\subsection{Problem decomposition}

Association rules mining problem is usually decomposed into two subproblems. First of all, it is needed to find all the frequent itemsets exceeding the minimum support. Secondly, when these itemsets are found, they are used to generate association rules satisfying the minimum confidence constraint. The second subproblem has a straightforward solution, when the rules are generated by iteratively moving the items from antecedent to consequent and checking the confidences of the generated rules for each frequent itemset. For this reason, the association rules mining problem can be reduced to finding all the frequent itemsets satisfying the minimum support constraint. 

\subsection{Apriori algorithm and its variations}

One of the most important algorithms for association rules mining is Apriori described by Agrawal and Srikant in \cite{Agrawal94}. Since its introduction, a lot of papers discussing Apriori variations and adaptations for different use cases were presented.

Apriori is a level-wise breadth-first algorithm that employs candidate sets generation and utilizes the downward closure property of itemset support. This algorithm works in a following way \cite{Agrawal94}. During the first pass, it calculates support of every single item occurring in a database. Those items that have support larger than minsup are considered \emph{large 1-itemsets}. All the subsequent passes consist of two phases. During the first phase, large itemsets $L_{k-1}$ found at previous pass are used to generate candidate itemsets $C_k$. During the second phase the support of the candidate itemsets is calculated and only those itemsets that have support larger than minsup are added to $L_k$. The algorithm continues until there are no new itemsets having support that is large enough. At the end, all large itemsets $L_k$ are combined to form final result.

The downward closure property of Apriori algorithm means that any subset of a large itemset must be large. Because of this property, it is only sufficient to generate candidate itemsets at pass $k$ by joining frequent itemsets obtained at pass $k\mhyphen1$ which is less computationally intensive than bruteforce of all existing k-itemsets. 

Limiting number of candidate k-itemsets and relatively fast pruning procedure responsible for removing the candidate k-itemsets not having sufficient support make Apriori an effective association rules mining algorithm for the market basket analysis. Apriori has satisfactory performance even if processing large datasets containing tens of thousands items and millions of transactions. 

However, Apriori has a number of drawbacks. One of the most important ones is a need to to scan the whole database at each pass. To overcome this drawback, a number of algorithms reducing the number of database scans were developed. One of such algorithms is AprioriTid, described together with Apriori in \cite{Agrawal94}. In original Apriori, the support of each candidate itemset is calculated out of raw data contained in the transactions. AprioriTid associates the transactions with the itemsets contained in them and calculates supports for the candidate itemsets based on these data. AprioriTid is more effective than Apriori for mining datasets of medium and small sizes that contain a number of medium-sized association rules. Apriori is more efficient when the interesting rules are short. AprioriHybrid \cite{Agrawal94} is a combined algorithm that switches between Apriori and AprioriTid implementations in different passes and usually has better performance than Apriori and AprioriTid on their own.

Another drawback of Apriori not allowing to effectively utilize this algorithm for certain data mining tasks is that Apriori often generates way too many short association rules that are not always interesting. When the length of the rules grows, their number may increase very fast for certain datasets. This makes Apriori and its variations rather inefficient for detecting relatively long patterns. AprioriTid and some other Apriori variations may be more efficient for this task, but their applicability remains rather limited anyway, because all of them are based on breadth-first traversal of a search space. 

There were many attempts to improve the performance of Apriori algorithms. The most notable techniques are reduction of database passes number, database sampling, imposing additional constrains on generated patterns, organizing items into taxonomies and algorithm parallelization \cite{Hegland,Kotsiantis}. Despite these optimizations are rather efficient when working with market basket-like data, they do not overcome architectural limitations of Apriori algorithm and thus do not help much with mining long rules.

\subsection{FP-growth algorithm}

Taking into account the above-mentioned drawbacks of Apriori-like algorithms, Han et al. have developed an entirely different technique of association rules mining that does not use candidate itemsets generation. This technique was introduced in \cite{Han00} and is known as \emph{frequent-pattern growth (FP-growth)}. Contrary to Apriori and its variations, FP-growth is a depth-first algorithm that scans the database only twice. It stores the needed intermediary information about the data in a condensed and small data structure called FP-tree. FP-growth is not the first developed depth-first association rules mining algorithm, but it is the most well known one.

FP-growth has three important features that distinguish it from Apriori-like algorithm \cite{Han00-2,Han03}. First of all, it maintains groupings of original data in FP-tree. Secondly, FP-growth uses divide-and-conquer technique to partition the dataset and the set of patterns to be examined. Thirdly, it avoids costly generation of candidate 
itemsets.

FP-growth algorithm for finding frequent itemsets consists of two steps \cite{Han03}. In the first step, a source transaction database is compressed to a FP-tree. In the second step, this structure is used to detect the itemsets.

The first step contains two database scans. At first, the database is scanned to derive a list of frequent items with their supports. Then, the items are sorted in decreasing support order. An empty tree with a null root is created. After these preliminary steps, the algorithm scans the database for the second time and for each transaction it creates a path in the tree going from the item with the highest support to the least frequent item with the lowest support. For each node in the tree, the algorithm keeps item name, count of item occurrences in the respective path and a node-link. If two transactions share the same path, either completely or partially (have common prefix), the algorithm increases count value for each of the shared items.

Simultaneously with building the tree, the algorithm creates an item header table where each item is pointed to its first occurrence in the tree. Nodes with the same item name are organized into linked lists via node-links. 

The resulting FP-tree is very compact for most databases, but still contains all the information about the transactions that is needed for association rules mining. Also, because of the node-links it is possible to retrieve all the transactions containing a certain item very fast. 

After the FP-tree is built, the algorithm derives the frequent itemsets from it using the following procedure. For a database with $N$ frequent items, the whole space of potential frequent itemsets is divided into $N$ parts. The first part contains only the itemsets having item $a_1$, the second contains the itemsets having $a_2$ but not having $a_1$ and so on up to the $N$th part containing only the itemsets with $a_N$ and no other items. Each of these parts can be traversed the following way.

Using the item header table and the node-links the algorithm retrieves all the transactions in which $a_i$ participates from an FP-tree. The prefixes of the paths where $a_i$ occurs form a sub-database which is called $a_i$'s conditional database. This database is used as a source for a new and smaller FP-tree. This process continues recursively until the conditional FP-tree is small enough for direct traversal. All combinations of its components are the valid frequent itemsets for the sub-database containing the transactions with $a_i$ element. To get the final result, it is needed to merge all the frequent itemsets obtained during the algorithm execution into one set. During the derivation of sub-databases, there is no need to include the already traversed paths for the following items.

\section{Performance testing challenge}

Foo et al. use Apriori algorithm for automatic detection of performance degradations in web systems \cite{Foo}. However, taking into account the drawbacks of Apriori that were discussed earlier in this chapter, it is doubtful that this algorithm is efficient for data mining performance testing data in industrial setting. Apriori is suitable for mining large datasets for short association rules, but the organization of performance data is usually different and due to some of its properties it is hard to manage by Apriori algorithm.

\subsection{Organization of performance data}

Contrary to market basket data where transactions have random structure, the structure of transactions in a performance testing database as it is implemented in a performance repository is rather formal. The market basket transactions may be of different length and usually contain almost entirely random items from a large item set. A transaction in a testing database contains a list of performance counters that are tracked for an application and a category for each of the counters. Each transaction contains all the available counters but their categories differ. The length of the transactions is the same. An example of a transaction database containing classified values of performance counters is presented in Table \ref{table:perf_db_example}.

\begin{table}
  \begin{center}
    \begin{tabular}{ | c | c | c | c | }
  	  \hline
  	  \bf{TID} & \bf{CPU util.} & \bf{DB reads} & \bf{DB writes} \\
      \hline
      1 & Medium & Medium & Medium \\ \hline
      2 & Medium & High & Medium \\ \hline
      3 & Medium & Medium & Medium \\ \hline
      4 & Low & Medium & Medium \\
      \hline
    \end{tabular}
    \caption{An example of a testing database.}
    \label{table:perf_db_example}
  \end{center}
\end{table}

\subsection{Impact of data organization to mining algorithms}

Organization of performance testing data allows to make certain assumptions concerning the association rules that are contained in performance datasets. First of all, the transactions in the datasets are usually rather long. More performance counters are tracked for the applications, the longer transactions are created in a system. This means that the database may contain very long association rules that are extremely difficult to calculate if following level-wise bottom-up breadth-first traversal of a search space. 

Also, the performance of association rules mining algorithm depends significantly from the chosen classification algorithm. If a taxonomy contains a small number of categories, then many baseline transactions will be very similar and this would lead to appearance of one or more extremely long rules. In this situation, there should be a way to detect these rules quickly and do not traverse the smaller itemsets contained in the long rules. On the other hand, if the classification algorithm would operate with many different categories, the resulting frequent itemsets should be smaller and have smaller support. The problem with this classification is that certain groupings of items can be lost. In the case study we compare the results obtained with different classification algorithms to decide which of them has better impact on the data mining algorithm.

Another important observation is that due to relatively small number of aggregated transactions representing a test run, minimum support value has to be rather high -- starting from several percents. Otherwise the results will suffer from noise patterns that are not observed in most part of data and are introduced mainly because of fluctuations not related to behavior of the software under study.

Due to the structure of data, the items contained in a performance database can be treated differently. In this study we consider each combination of a performance counter and a category associated with it as a separate item. E.g. counter \emph{CPU utilization} with value \emph{Low} and the same counter with value \emph{Medium} are treated as two entirely different items. This approach to data processing allows to use association rules mining the same way this technique is applied in the market basket analysis, as all the items in the item set are different and are not related in any way. The drawback of this approach is that we do not benefit from the additional information contained in the data, namely from its structure. We know, that each transaction should contain exactly one copy of each item with a value from a taxonomy. Use of this restriction should help with traversing the search space and we will try to do it in the case study. 

\subsection{Items hashing}

Before applying the association rules mining algorithms discussed in the following chapter, we convert the items appearing in the transactions to integers using a simple two-way hashing function. After the rules are derived, the original counter-category relations are restored. This is a performance optimization aimed to speed up items' comparisons.

To keep the additional information about the performance data strucure in the converted items, it was needed to find a hashing function that would be aware of it and allow to recover this information during decoding. For this reason, we use the following hashing function to encode data: \(e = counter * N + category\). In this function, $item$ is an ID of a counter in a database, $category$ is an ID of a category in the taxonomy used for the counter and $N$ is a multiplier exceeding number of categories (we used 10 in our experiments).

To decode data, we divide $e$ by $N$. After a division, $counter$ ID is the quotient and $category$ ID is the remainder. After applying this hashing function, the resulting values are ordered first by the respective counter ids, then by category ids.

\subsection{Datasets comparison problem}

As we have stated earlier in this chapter, the main problem that has to be solved during the analysis of performance testing datasets is finding the degree of similarity between them. To decide, whether a target run resembles baseline runs with association rules mining, it is needed to find patterns in both datasets and compare them. For performance testing use case the most important task during the comparison is to find the patterns that appear in baseline dataset, but are missing in target, or appear in both datasets but have different characteristics. Those patterns that appear in the target run but are missing in the baseline runs are less interesting, as they help to answer whether the baseline resembles the target, and we are interested in vice versa.

The existence of two different datasets imposes a certain challenge, as usually the problems to be solved with the association rules involve only one dataset. After the patterns are found in it, they are reported to the analysts and data mining ends. If there are two datasets, the patterns have to be detected in both of them and compared afterwards. Apart from being important for performance testing, this problem appears e.g. in market basket analysis when there is a need to analyze how the customers' behavior changes over time or at different locations. However, to the best of our knowledge, not much work was done previously on finding differences in patterns appearing in transaction databases. Thus, Tansel and Ayan refer to this problem in \cite{Tansel} but do not suggest an algorithm for comparing large sets of frequent itemsets or association rules. Foo et al. in \cite{Foo} discuss an algorithm to detect performance degradations using association rules generated with Apriori algorithm. In their approach, the researchers derive association rules for the baseline dataset, apply them to target dataset and make conclusions on the validity of the rules based by the degree of deviation between the correlation confidence for baseline and target runs. 

In the next chapter we present a new algorithm for performance testing datasets mining that compares maximal frequent itemsets to quickly find the similarities between baseline and target runs in a performance test.

\begin{thebibliography}{99}
\bibitem{Agrawal93} Rakesh Agrawal, Tomasz Imielinski and Arun Swami. Mining Association Rules between Sets of Items in Large Databases. In \emph{Proc. ACM SIGMOD'93}, Washington DC, USA, 1993.
\bibitem{Agrawal94} Rakesh Agrawal and Ramakrishnan Srikant. Fast Algorithms for Mining Association Rules. In \emph{Proc. 20th VLDB}, Santiago, Chile, 1994.
\bibitem{Fayyad} Usama M. Fayyad, Gregory Piatetsky-Shapiro, Padhraic Smyth. From Data Mining to Knowledge Discovery in Databases. In \emph{AI Magazine} 17(3), pp. 37-54, 1996.
\bibitem{Foo} King Chun Foo, Zhen Ming Jiang, Bram Adams, Ahmed E. Hassan, Ying Zou, and Parminder Flora. Mining performance regression testing repositories for automated performance analysis. In \emph{Proc. 10th Int’l Conf. on Quality Software (QSIC’10)}, pp. 32–41, Zhangjiajie, China, 2010.
\bibitem{Han00} Jiawei Han, Jian Pei and Yiwen Yin. Mining Frequent Patterns without Candidate Generation. In \emph{Proc. ACM SIGMOD'00}, Dallas, TX, USA, 2000. 
\bibitem{Han00-2} Jiawei Han and Jian Pei. Mining frequent patterns by pattern growth: Methodology and implications. In \emph{SIGKDD Explorations} 2(2), pp. 14–20, 2000.
\bibitem{Han03} Jiawei Han, Jian Pei, Yiwen Yin and Runying Mao. Mining Frequent Patterns without Candidate Generation: A Frequent-Pattern Tree Approach. In \emph{Data Mining And Knowledge Discovery} 8, pp. 53-87, 2003.
\bibitem{Hegland} Markus Hegland. Algorithms for Association Rules. In \emph{Lecture Notes in Computer Science} 2600, pp. 226-234, 2003.
\bibitem{Kotsiantis} Sotiris Kotsiantis, Dimitris Kanellopoulos. Association Rules Mining: A Recent Overview. \emph{GESTS International Transactions on Computer Science and Engineering} 32(1), pp. 71-82, 2006.
\bibitem{Tansel} Abdullah Uz Tansel and Necip Fazil Ayan. Discovery of Association Rules in Temporal Databases. In \emph{Proc. AAAI Knowledge Discovery in Databases}, 1998.
\end{thebibliography}

\end{document}