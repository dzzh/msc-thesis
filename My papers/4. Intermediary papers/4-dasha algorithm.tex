\documentclass[a4paper, notitlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{subfig}
\usepackage{tikz}
\usepackage{listings}
\usepackage{color}
\usepackage{caption}

\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{gray}{\parbox{\textwidth}{#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}
\definecolor{lightgray}{gray}{0.9}

\usetikzlibrary{trees}
\mathchardef\mhyphen="2D

\lstset{
  language=Python,
  basicstyle=\small\ttfamily,
  commentstyle=\color{gray},
  tabsize=4,
  numbers=left,
  numberstyle=\tiny,
  stepnumber=1,
  numbersep=5pt,
  backgroundcolor=\color{lightgray},
  keywordstyle=\color{blue},
  }

\begin{document}

\title{Dasha: a performance testing mining algorithm}  
\author{Zmicier Zaleznicenka, MSc CS student, \#4134575}
\date{\today}
\maketitle

In this chapter we present an algorithm called Dasha to compare the patterns existing in transaction databa\-ses with association rules mining techniques. In Dasha, the datasets are initially compared using the frequent itemsets that are derived from them. Only after the itemsets are compared and their difference is obtained, this difference is used to construct association rules for the further analysis. This two-level comparison approach based on traversal of lexicographic tree structures allows to compare large performance testing datasets in reasonable time and is suitable for usage in industrial setting.

\section{Dasha overview}

Taking into account the considerations listed in the previous chapter, Dasha algorithm was designed with the emphasis on performance. As we have pointed out earlier, number of association rules extracted from the performance databa\-ses can be extremely large. Most of these rules are not interesting and are contained in longer rules. Their evaluation leads to significant time consumption even for rather small databases. To mitigate this problem, we suggest not to start the analysis with the association rules generation, but rather extract and analyze frequent itemsets at first. We introduce a tree structure that is intended to keep the frequent itemsets in a compact form and which allows to compare them in reasonable time. 

In the proposed algorithm we use a tree structure to store the information about the frequent itemsets and their supports. This tree serves as a caching layer for itemsets-related information and implements a data structure initially described by Rymon as a \emph{set-enumeration tree} in \cite{Rymon}. Prior to our study, set-enumeration trees have already been used for association rules mining by a number of researchers (see e.g. \cite{Agarwal,Bayardo,Burdick}). However, in the preceding studies such trees were mainly used to store the information about the transactions contained in the source datasets for further generation of the frequent itemsets. Agarwal et al. in \cite{Agarwal} use set-enumeration tree to store itemset-related data, but their implementation significantly differs from an approach presented in our study. A contribution of our approach is an idea to construct a set-enumeration tree from the already retrieved frequent itemsets and use the resulting data structure for quick traversal of the itemsets, retrieval of their support and fast comparison of different sets of frequent itemsets. 

Later in this paper we refer to the set-enumeration tree structure used for storing frequent itemsets as to \emph{support tree}, because it stores supports for the combinations of items appearing in the datasets under study.

Dasha algorithm to detect performance degradations consists of the following steps.

\begin{enumerate}
\item Build support trees for baseline and target datasets. 
\item Compare the support trees using level-wise top-down approach.
\item Derive the association rules from the different frequent itemsets.
\item Analyze the association rules and make conclusions.
\end{enumerate}

In the following sections we describe these steps in more details and explore the properties of the resulting algorithm.

\section{Sample datasets}
\label{sec:sample_datasets}

To illustrate the flow of Dasha algorithm and the data structures used in it, throughout this chapter we demonstrate the algorithm execution on a simple example. In this section we describe the source data that is used in it.

Assume we would like to find the difference in performance between two test runs. The first run is performed on a stable version of the software and has satisfactory performance. It is considered to be a baseline. The second run is performed on a new version of the software and its performance has to be evaluated. Both test runs consist of six observations, during each observation four performance counters are tracked. The data is discretized and stored in a performance repository. Thus, each counter value is associated with a category, either \emph{Low} (1), \emph{Medium} (2) or \emph{High} (3). The counter values are hashed to improve the algorithm performance. As a result, each counter observation in the datasets has value between 11 and 43, where tens mean the IDs of the performance counters, ones mean categories. E.g. 12 means \emph{Medium} value for counter \emph{c1}, 43 means \emph{High} value for counter \emph{c4}. The performance datasets contents after these optimizations are presented in Table \ref{table:sample_datasets}. 

\begin{table}[ht]
  \begin{center}
  \subfloat[baseline]{
    \begin{tabular}{ | c | c | c | c | c | }
  	  \hline
  	  \bf{TID} & \bf{c1} & \bf{c2} & \bf{c3} & \bf{c4} \\
      \hline
      1 & 12 & 22 & 32 & 42 \\ \hline
      2 & 12 & 22 & 32 & 42 \\ \hline
      3 & 12 & 23 & 32 & 43 \\ \hline
      4 & 12 & 22 & 32 & 42 \\ \hline
      5 & 12 & 22 & 32 & 42 \\ \hline
      6 & 12 & 23 & 32 & 43 \\ 
      \hline
    \end{tabular}}
    \quad
    \subfloat[target]{
    \begin{tabular}{ | c | c | c | c | c | }
  	  \hline
  	  \bf{TID} & \bf{c1} & \bf{c2} & \bf{c3} & \bf{c4} \\
      \hline
      1 & 12 & 22 & 32 & 42 \\ \hline
      2 & 12 & 23 & 32 & 43 \\ \hline
      4 & 12 & 22 & 32 & 42 \\ \hline
      3 & 12 & 23 & 32 & 43 \\ \hline
      5 & 12 & 23 & 32 & 41 \\ \hline
      6 & 12 & 22 & 32 & 42 \\ 
      \hline
    \end{tabular}}
    \caption{Sample datasets.}
    \label{table:sample_datasets}
  \end{center}
\end{table}

As this example is trivial, the conclusions about differences in data can be made manually. First of all, it is obvious that counters \emph{c1} and \emph{c3} are stable in both datasets. In every observation they have \emph{Medium} value that is encoded as 12 and 32 respectively. Counter \emph{c2} has \emph{High} value in two observations in the baseline dataset and in three observations in the target dataset. It has \emph{Medium} value in four observations in the baseline dataset and in three observations in the target dataset. Counter \emph{c4} behaves similarly to \emph{c2} in the baseline but has different behavior in the target dataset. It has \emph{Low} value in one observation, \emph{Medium} value in three observations and \emph{High} value in two observations. Depending on the algorithm settings (i.e. \emph{minsup} and \emph{mindiff}) these differences can either be reported as violations or not. 

\section{Support tree structure and generation}
\label{sec:support_tree}
Before discussing the algorithm implementation, we have to describe the support tree that is an important data structure used in Dasha.

This tree stores the information about the frequent itemsets existing in the studied database and their supports. It is assumed that the items within a frequent itemset do not repeat and there is total ordering $<_O$ of the items in the database. If item $i$ occurs before item $j$ in the ordering, this is denoted by $i<_Oj$. 

Support tree closely resembles the FP-tree used for frequent itemsets generation in FP-growth algorithm described in \cite{Han00}, but differs from it in the following aspects.

\begin{itemize}
\item FP-tree is used to organize a list of transactions. Support tree is used to organize a list of frequent itemsets.
\item Support tree does not have a concept of an item-header table.
\end{itemize}

In other aspects, the support tree resembles the FP-tree and can be built following the procedure similar to building the FP-tree. Total ordering in the support tree is also implemented similarly to the ordering used in the FP-tree. Namely, the items in an itemset to be added to the support tree are first sorted by their frequency in decreasing order, then naturally in increasing order. Later in this document we refer to this ordering as to \emph{FP-ordering}.

More formally, the support tree can be defined as follows\footnote{Tree definition and algorithm description are based on the FP-tree description as presented in \cite{Han00}.}.
\newline\newline
\textbf{Definition 1 (Support tree).} A \emph{support tree} is a data structure defined below. 

\begin{enumerate}
\item Support tree is a tree structure. It consists of a root node containing \emph{null} as an item and a set of item-prefix subtrees as the children of the root.
\item Each node in the item-prefix subtree contains an underlying item, support value, link to a node parent and a set of links to node children.
\item Support value of a node is used to store the support of an itemset that appears in the respective dataset and includes the node and all its parents up to the root.
\item For two nodes $i$ and $j$ in the support tree it holds that if $i.parent == j$, then $j.item <_O i.item$ and $i.support \le j.support$.
\end{enumerate}

Based on this definition, a support tree can be constructed using the following algorithm.
\newline\newline
\emph{Input:} a list of itemsets.\newline
\emph{Output:} an instance of a support tree.\newline

Initialize a support tree with a root $R$ associated with \emph{null} item. For each frequent itemset in the input list do the following.
\begin{enumerate}
\item Sort the items appearing in the frequent itemset in FP-order using $<_O$ relation. Let the list of items in a current itemset be $[p|P]$, where $p$ is the first element and $P$ is the remaining list. 
\item Add the items into the tree using $R.add([p|P])$ procedure that works as follows. If $R$ has a child $n$ such that $n.item == p.item$, remember this node; otherwise create a new node $n$ and link its parent to $R$. Then, if $P$ is non-empty, call $n.add(P)$; otherwise, set $n.support$ to itemset support.
\end{enumerate} 

\begin{center}
\line(1,0){250}
\end{center}

During the tree construction process, the itemset supports are assigned to the last nodes of the respecting paths in the tree. If the source list includes all the frequent itemsets that exist in the database, the resulting support tree will not have nodes with no support associated with them, except for the root. 

\section{Dasha implementation}

In this section we discuss the steps used in Dasha algorithm in more details.

\subsection{Generation of the support trees}

\emph{Input:} transaction database, minsup, mindiff.\newline
\emph{Output:} support tree.

\subsubsection{Frequent itemsets generation and support tree construction}

The first step of Dasha algorithm to compare the patterns in transaction databases is to construct the support trees for both baseline and target datasets.

As the support tree construction is based on processing of frequent itemsets, they have to be generated from the database in advance. To complete this task, any existing frequent itemsets mining algorithm can be used. In our tests we have used an implementation of FP-growth algorithm \cite{Han00}. This algorithm was chosen because of the structure of performance testing datasets that was described in the previous chapter. This structure imposes existence of large frequent itemsets in the database and FP-growth should be more efficient for generating them than any of Apriori-based algorithms. Also, the FP-trees that are generated by FP-growth algorithm are used at the later steps in Dasha for calculating itemset supports.

After a list of frequent itemsets with their supports is retrieved from the database, it is used to build a support tree as described in section \ref{sec:support_tree}.

\subsubsection{Decreasing minimum support value for the target tree}

Before generating a support tree for the target dataset, it is needed to decrease its minimum support to include in the resulting support tree the itemsets that have a little lower support than the ones in a baseline dataset. This change is important for a later step when the support trees are compared. If not decreasing target's minimum support, there will be situations when an itemset $A$ has support slightly higher than minsup in the baseline dataset and slightly lower than minsup in the target dataset. In this case this itemset should appear in the baseline support tree but it will be missing from the target support tree and during the comparison this will be reported as a violation. However, if the target dataset would contain the itemsets with support lower than minsup, itemset $A$ will be presented in it and the comparison of these itemsets will be performed based on the difference between their support values, that can be insignificant. 

It is important to properly select the decreased value for target's minimum support. If this value is chosen to be too close to minsup threshold specified by the user, it will not solve the problem. If target's minsup is too low, this will lead to the drastic drop of the algorithm performance, as when minsup decreases, the number of frequent itemsets increases and it takes longer to generate them. In our experiments, we used the following formula to calculate minsup value for the target dataset.

\begin{center}
$minsup_{target}=minsup*(1-mindiff)$
\end{center}

Here, $minsup$ is minimum support threshold measured in number of transactions and $mindiff$ is minimum difference threshold measured from 0 to 1. These values are defined by user. The meaning of $mindiff$ is explained in section \ref{sec:support-comparison}.

\subsubsection{Implementation}

Pseudocode implementation of support trees generation algorithm is presented in Listing \ref{lst:build-trees}.

\lstinputlisting[language=Python,caption=Support trees construction., label={lst:build-trees}]{code/dasha-build-trees.py}

\subsubsection{Example}

The resulting support trees generated for the sample datasets with Dasha algorithm are presented at Figure \ref{fig:support_trees}. A set of paths in each support tree equals to the set of frequent itemsets for each of the datasets. The trees were generated with $minsup=2$ and $mindiff=10\%$. Each node in the resulting tree contains a value from the dataset and its support in parenthesis. The support is calculated for the itemset containing the node itself and all its parents up to the root.

In this example the target support is not adjusted as both $minsup$ and $mindiff$ values are rather small. With such settings decreasing target support is not helpful.

From the support trees we can easily see that the itemsets $\{12,22\}$, $\{12,23\}$, $\{42\}$, $\{43\}$ and some others have different supports and thus represent potential violations. Whether these differences will be reported as violations or not depends on the algorithm settings (\emph{minsup}, \emph{mindiff}). 

Shaded nodes in the baseline support tree illustrate the roots of the subtrees that differ from target. They are explained in section \ref{sec:support-comparison-example}. 

\begin{figure}
\centering
\subfloat[baseline]{  
\tikzstyle{every node}=[anchor=west]
\tikzstyle{border}=[fill=black!20]
\begin{tikzpicture}[%
  grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
  edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)},
  scale=1.0]
  \node {root}
    child { node {12(6)}
    	child { node [border] {22(4)}
    		child { node {42(4)}}
    	}
      child [missing] {}
      child { node [border] {23(2)}
        child { node {43(2)}}
      }
    	child [missing] {}    	
    	child { node {32(6)}
    		child { node [border] {22(4)}
    			child { node {42(4)}}
    		}
    		child [missing] {}		
    		child { node [border] {23(2)}
          child {node {43(2)}}
        }
        child [missing] {}
        child { node [border] {42(4)}}
        child { node {43(2)}}
    	}
    	child [missing] {}
    	child [missing] {}
    	child [missing] {}    	
      child [missing] {}
      child [missing] {}            
      child [missing] {}      
    	child { node [border] {42(4)}}
    	child { node {43(2)}}
    }
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    	
    child { node [border] {22(4)}
    	child { node {42(4)}}
  	}
    child [missing] {}    
    child { node [border] {23(2)}
      child { node {43(2)}}
    }  
    child [missing] {}    	
    child { node {32(6)}
    	child { node [border] {22(4)}
    		child { node {42(4)}}
    	}
    	child [missing] {}		
    	child { node [border] {23(2)}
        child { node {43(2)}}
      }
      child [missing] {}    
      child { node [border] {42(4)}}
      child { node {43(2)}}
    }		
    child [missing] {}    
    child [missing] {}				
    child [missing] {}		
    child [missing] {}				
    child [missing] {}
    child [missing] {}    
    child { node [border] {42(4)}}
    child { node {43(2)}};
\end{tikzpicture}}
\subfloat[target]{
\tikzstyle{every node}=[anchor=west]
\begin{tikzpicture}[%
  grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
  edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)},
  scale=1.0]
  \node {root}
    child { node {12(6)}
      child { node {22(3)}
        child { node {42(3)}}
      }
      child [missing] {}
      child { node {23(3)}
        child { node {43(2)}}
      }
      child [missing] {}      
      child { node {32(6)}
        child { node {22(3)}
          child { node {42(3)}}
        }
        child [missing] {}    
        child { node {23(3)}
          child {node {43(2)}}
        }
        child [missing] {}
        child { node {42(3)}}
        child { node {43(2)}}
      }
      child [missing] {}
      child [missing] {}
      child [missing] {}      
      child [missing] {}
      child [missing] {}            
      child [missing] {}      
      child { node {42(3)}}
      child { node {43(2)}}
    }
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}    
    child [missing] {}      
    child { node {22(3)}
      child { node {42(3)}}
    }
    child [missing] {}    
    child { node {23(3)}
      child { node {43(2)}}
    }  
    child [missing] {}      
    child { node {32(6)}
      child { node {22(3)}
        child { node {42(3)}}
      }
      child [missing] {}    
      child { node {23(3)}
        child { node {43(2)}}
      }
      child [missing] {}    
      child { node {42(3)}}
      child { node {43(2)}}
    }   
    child [missing] {}    
    child [missing] {}        
    child [missing] {}    
    child [missing] {}        
    child [missing] {}
    child [missing] {}    
    child { node {42(3)}}
    child { node {43(2)}};
\end{tikzpicture}}
\caption{Support trees for the sample datasets.}
\label{fig:support_trees}
\end{figure}

\subsection{Support trees comparison}
\label{sec:support-comparison}

\emph{Input:} Support trees and FP-trees for baseline and target, mindiff.\newline
\emph{Output:} a list of violating itemsets.

\subsubsection{Comparison algorithm description}

After the baseline and target support trees are constructed, they have to be compared to find the difference in patterns appearing in the respective datasets.

To find the difference between the support trees we use a top-down breadth-first approach. The comparison starts with the children of the root node in the baseline support tree. These nodes are added into the comparison queue and until the queue is empty the nodes are retrieved from it one by one and processed.

Once a baseline tree node is retrieved from a comparison queue, the algorithm looks for the corresponding node in the target tree. The support tree is constructed in such a way that every node in it represents an itemset including the node itself and all its parents up to the root. The support value of a node equals to the support of this itemset. Hence, to find a node $A_T$ in a target tree that corresponds to a node $A_B$ in a baseline tree, it is needed to move up the baseline tree to reconstruct an ordered itemset $A$ and find this itemset in the target tree by moving down from the root.

If a lookup procedure is not able to find $A_T$ node, this has to be considered a violation. In this case, even if $A_B$ node has any children, the lookup process for the subtree having $A_B$ as a root should stop, because all the itemsets identified with $A_B$'s children contain $A_B$'s item, and if a target tree does not contain $A_T$, it neither contains any itemsets including its item, which is the same as $A_B$'s item. 

If $A_T$ is found, the algorithm compares the support values of $A_B$ and $A_T$. Before the comparison, if the number of transactions in the baseline and target datasets differs, the support value in the target tree has to be properly scaled to reflect this difference.

To compare the support values for the support tree nodes, minimum difference value is used as a threshold. If $\frac{|support_{A_B}-support_{A_T}|}{support_{A_B}}$ exceeds $mindiff$, it is assumed that the values are significantly different and this is a violation. In this case, the algorithm reacts the same way as if $A_T$ is missing. Any $A_B$'s children are not processed further.

If the difference between $A_B$ and $A_T$ is less that $mindiff$, it is assumed that pattern $A$ matches in the support trees. In this case, all $A_B$'s children are added to the queue.

After the queue is empty, the algorithm reports a list of violating itemsets detected when processing the items in the queue. These itemsets are grouped by their parent non-violating itemsets (we call itemset ${a,b}$ a parent of FP-ordered itemset ${a,b,c}$).

\subsubsection{Implementation}

Pseudocode of support trees comparison algorithm can be found in Listing \ref{lst:compare-trees}.

\lstinputlisting[language=Python,caption=Support trees comparison., label={lst:compare-trees}]{code/dasha-compare-trees.py}

\subsubsection{Example}
\label{sec:support-comparison-example}

At Figure \ref{fig:support_trees} there are the support trees that are built from the sample baseline and target datasets presented in section \ref{sec:sample_datasets}. The shaded nodes in the baseline trees illustrate the difference between the trees that is found if the algorithm is run with $minsup=2$ and $mindiff=10\%$. If the difference is found for a node, the algorithm does not go further to analyze its children. 

The results of comparing the support trees for the sample datasets are presented in Listing \ref{lst:support-trees}.

\begin{lstlisting}[caption=Comparison of support trees for sample datasets.,label={lst:support-trees}]
Shared path: [] (supports 6, 6)
  node 22 (supports 4, 3)
  node 23 (supports 2, 3)
  node 42 (supports 4, 3)

Shared path: [12L] (supports 6, 6)
  node 22 (supports 4, 3)
  node 23 (supports 2, 3)
  node 42 (supports 4, 3)

Shared path: [32L] (supports 6, 6)
  node 22 (supports 4, 3)
  node 23 (supports 2, 3)
  node 42 (supports 4, 3)

Shared path: [12L, 32L] (supports 6, 6)
  node 22 (supports 4, 3)
  node 23 (supports 2, 3)
  node 42 (supports 4, 3)
\end{lstlisting}

\subsection{Generation of association rules and their analysis}

\emph{Input:} a list of violating itemsets, fp-trees, mindiff.\newline
\emph{Output:} a list of association rules grouped by antecedent.

\subsubsection{Association rules generation}
\label{sec:assoc-rules}

After a list of violating itemsets is retrieved, it is used for the generation of the association rules. The itemsets presented in the input list significantly differ from their parents and this allows to conclude that the association rules generated out of the violating itemsets should reflect this difference. For each violating itemset presented in the input list and containing $n$ items, Dasha algorithm generates $n$ association rules having $n-1$ items in the antecedent and one distinct item in the consequent. For each of the generated rules, the algorithm calculates its support and confidence. For rule $[a,b] \rightarrow c$, support is calculated as $support_{[a,b,c]}$, confidence is calculated as $\frac{support_[a,b]}{support_{[a,b,c]}}$.

An important challenge to be addressed by the algorithm is fast calculation of rule confidences. When the support of a rule can be found in the respective support tree, the support for the rule antecedent is not necessarily presented there. For example, if an algorithm finds a violating itemset $\{a,b,c\}$, it should generate three association rules out of it, namely

\begin{center}
$[a,b] \rightarrow c$,

$[a,c] \rightarrow b$,

$[b,c] \rightarrow a$.
\end{center}

Obviously, depending on the source data the itemsets $\{a,c\}$ and $\{b,c\}$ may be infrequent and thus missing from the respective support tree. A naïve approach to calculate their supports would be to count their occurrences in the source datasets. However, this would drastically decrease the algorithm performance. Instead, Dasha algorithm retrieves these data from the corresponding FP-trees. To do it, the algorithm takes the least frequent item from the itemset, finds all its appearances in the FP-tree using its node-links and moves up to the root starting from these nodes. If the algorithm passes all the itemset nodes on its way to the root, it concludes that the respective path is valid. Then it summarizes the count values for the least frequent nodes in the valid paths. By FP-tree design, the resulting value equals to the itemset support in the respective dataset.

A list of association rules generated by Dasha algorithm for the sample datasets is presented in Listing \ref{lst:association-rules}. The association rules generated at this step are grouped by their antecedent and are subject to additional optimizations that are discussed in the following section. 

\subsubsection{Association rules analysis and selection}
\label{sec:assoc-rules-analysis}

The resulting list of association rules reflects the most important differences between the baseline and target datasets. Level-wise top-down comparison of frequent itemsets organized in a form of a set-enumeration tree has allowed to limit the number of the association rules that are returned. However, the number of these rules can be decreased even more. The algorithm generates many rules with the same consequents, but different antecedents. To detect a violation for the node represented by the association rule consequent, the amount of the returned data may be excessive. To address this issue, the algorithm chooses one the most representative rule per different consequent instead of returning all the association rules generated at the previous step. The selection criteria is average difference between baseline and target supports for the categories of a given counter in a rule (called rule severity). More formally, it is measured as \(\frac{\sum_{i=1}^{c}|c_i.bsl\_sup-c_i.tgt\_sup|}{r.bsl\_sup+r.tgt\_sup}\), where $r$ is an association rule, $c$ -- rule consequents for the violating performance counter. If severity metric is almost the same for the two rules, the rule with larger baseline support is selected for further reporting. For example, if the algorithm generates three association rules for consequent $d$, namely 
\begin{center}
$[a] \rightarrow d$ (severity 0.15, baseline support 0.4), 

$[a,b] \rightarrow d$ (severity 0.2, baseline support 0.3), 

$[a,b,c] \rightarrow d$ (severity 0.2, baseline support 0.2), 
\end{center}
the second rule will be chosen for reporting as it has higher severity than the first rule and higher baseline support than the third rule. In section \ref{sec:assoc-rules-example} we show the result of applying the same heuristic to the association rules generated from the sample datasets.

After the most descriptive association rule is chosen for each of the violating performance counters, it makes sense to present also the distribution of categories for this counter. This can be helpful in making conclusions about the violation -- whether the counter values in the target dataset increased or decreased in comparison with the baseline. This can be done using the FP-tree traversals similarly to calculating itemset supports as described in section \ref{sec:assoc-rules}. The traversal will return all the categories of a given rule consequent and their supports. When returning these data, some categories with low supports in baseline and target can be dropped to emphasize the most important categories. 

Listing \ref{lst:final-output} shows the output of Dasha algorithm for the sample datasets after the performed optimizations. This is the final output of the algorithm that is used by the report generation engine described in the following chapter.

\subsubsection{Implementation}

Pseudocode implementation of association rules generation algorithm is presented in Listing \ref{lst:build-rules}. Implementation of rules selection algorithm is presented in Listing \ref{lst:analyze-rules}.

\lstinputlisting[language=Python,caption=Association rules generation., label={lst:build-rules}]{code/dasha-build-rules.py}

\lstinputlisting[language=Python,caption=Association rules analysis., label={lst:analyze-rules}]{code/dasha-analyze-rules.py}

\subsubsection{Example}
\label{sec:assoc-rules-example}

Association rules generated for the sample datasets are presented in Listing \ref{lst:association-rules}.

\begin{lstlisting}[caption=Association rules for sample datasets., label={lst:association-rules}]
Antecedent: [] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)

Antecedent: [32L] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)

Antecedent: [12L] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)

Antecedent: [12L, 32L] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)
\end{lstlisting}

After performing the optimizations described in section \ref{sec:assoc-rules-analysis}, the output of Dasha algorithm looks as presented in Listing \ref{lst:final-output}. The optimizations performed to the association rules as they are shown in Listing \ref{lst:association-rules} include the following.

\begin{itemize}
\item Rules with duplicating consequents are removed. The resulting output contains only the rules with different consequents. 
\item For each violating counter the resulting output contains all possible categories that exist in the source datasets (see rule $[12,32]\rightarrow41$, which is missing in Listing \ref{lst:association-rules} due to its low support and rule $[12,32]\rightarrow43$ which is missing from Listing \ref{lst:association-rules} as it is considered matching in the datasets).
\end{itemize}

\begin{lstlisting}[caption=Final output of Dasha algorithm for sample datasets.,label={lst:final-output}]
Antecedent: [12L, 32L] (supports 6, 6)
 consequent 22: supports (4, 3), confidences (0.67, 0.50)
 consequent 23: supports (2, 3), confidences (0.33, 0.50)

Antecedent: [12L, 32L] (supports 6, 6)
 consequent 41: supports (0, 1), confidences (0.00, 0.17)
 consequent 42: supports (4, 3), confidences (0.67, 0.50)
 consequent 43: supports (2, 2), confidences (0.33, 0.33)
\end{lstlisting}

\subsection{Putting it all together}

Source code of Dasha algorithm can be found in Listing \ref{lst:dasha-algorithm}. It contains the functions described in the previous listings earlier in this chapter.

\lstinputlisting[language=Python,caption=Dasha algorithm to compare patterns in transaction databases., label={lst:dasha-algorithm}]{code/dasha.py}

\section{Conclusions}

In this chapter we presented Dasha, a data mining algorithm for fast detection of differences in the transaction databases using association rules mining techniques. This algorithm was designed to compare the results of performance test runs, but can also be applied in the other fields. For example, we expect the presented algorithm to be effective in market basket analysis for detecting changes in customer behavior in different locations or over time. However, this assumption has to be justified with a case study.

\begin{thebibliography}{99}
\bibitem{Agarwal} Ramesh C. Agarwal, Charu C. Aggarwal, and V.V.V. Prasad. A Tree Projection Algorithm for Generation of Frequent Item Sets. In \emph{J. Parallel and Distributed Computing} 61(3), pp. 350-371, 2001.
\bibitem{Bayardo} Roberto J. Bayardo Jr. Efficiently Mining Long Patterns from Databases. In \emph{Proc. ACM SIGMOD'98}, pp. 85-93, Seattle, WA, USA, 1998.
\bibitem{Burdick} Doug Burdick, Manuel Calimlim and Johannes Gehrke. MAFIA: A Maximal Frequent Itemset Algorithm for Transactional Databases. In \emph{Proc. 17th Int'l. Conf. on Data Engineering}, pp. 443-452, 2001.
\bibitem{Han00} Jiawei Han, Jian Pei and Yiwen Yin. Mining Frequent Patterns without Candidate Generation. In \emph{Proc. ACM SIGMOD'00}, Dallas, TX, USA, 2000. 
\bibitem{Rymon} Ron Rymon. Search through Systematic Set Enumeration. In \emph{Proc. 3rd Int'l Conf. Principles of Knowledge Representation and Reasoning}, pp. 539-550, 1992.
\end{thebibliography}

\end{document}